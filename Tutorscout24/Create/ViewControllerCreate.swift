//
//  ViewControllerCreate.swift
//  Tutorscout24
//
//  Created by Jan Becker on 19.10.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import UIKit
import Eureka

class ViewControllerCreate: FormViewController {
    
    //Provides the table for creating an offer or request
    override func viewDidLoad() {
        super.viewDidLoad()
        NavigationBarItems.initializeNavigationBar(navItem: self.navigationItem, title: "Erstellen")
        setForm()
    }
    
    func setForm(){
        form = Section("Inhalt der Anzeige")
            <<< TextRow(){
                $0.title = "Titel"
                $0.placeholder = "Max. 100 Zeichen"
                $0.tag = "title"
            }
            <<< TextAreaRow(){
                $0.title = "Beschreibung"
                $0.placeholder = "Beschreibung mit max. 500 Zeichen"
                $0.tag = "description"
            }
            +++ Section(header: "Nachhilfedauer", footer: "Die Dauer einer Nachhilfestunde.")
            <<< CountDownInlineRow() {
                $0.title = "Dauer"
                $0.value = Date(timeIntervalSince1970: 0)
                $0.tag = "duration"
            }
            /*+++ MultivaluedSection(multivaluedOptions: [.Insert, .Delete],
             header: "Stichwörter",
             footer: "Es können max. 25 Stichwörter zur Anzeige hinzugefügt werden.") {
             $0.addButtonProvider = { section in
             return ButtonRow(){
             $0.title = "Stichwort hinzufügen"
             }
             }
             $0.multivaluedRowToInsertAt = { index in
             return NameRow() {
             $0.placeholder = "Tag Name"
             }
             }
             $0 <<< NameRow() {
             $0.placeholder = "Tag Name"
             }
             }*/
            +++ Section("")
            <<< ButtonRow(){
                $0.title = "Anzeige erstellen"
                
                }.onCellSelection {  cell, row in
                    
                    //Create Request / Offer
                    let titleRow: TextRow? = self.form.rowBy(tag: "title")
                    let title = titleRow?.value
                    
                    let descRow: TextAreaRow? = self.form.rowBy(tag: "description")
                    let description = descRow?.value
                    
                    if(title == nil || description == nil){
                        let alert = UIAlertController(title: "Achtung", message: "Titel oder Beschreibung darf nicht leer sein!", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                        return
                    }
                    
                    let durRow: CountDownInlineRow? = self.form.rowBy(tag: "duration")
                    let durationDate = durRow?.value
                    let calendar = Calendar.current
                    let hour = calendar.component(.hour, from: durationDate!)
                    let minutes = calendar.component(.minute, from: durationDate!)
                    let duration = hour * 60 + minutes
                    
                    let latitude = OffersOrRequests.shared.getLatitude()
                    let longitude = OffersOrRequests.shared.getLongitutde()
                    
                    let keychain = KeychainSwift()
                    let username = keychain.get("username")
                    let password = keychain.get("password")
                    let authentication = Authentication(userName: username!, password: password!)
                    
                    let createOfferOrRequest = CreateOfferOrRequest(subject: title!, text: description!, duration: duration, latitude: latitude, longitude: longitude, authentication: authentication)
                    
                    var jsonUrl = JsonUrl.createRequest
                    var offorreq = "Request"
                    if(!GlobalVariables.sharedManager.getMode()){
                        jsonUrl = JsonUrl.createOffer
                        offorreq = "Offer"
                    }
                    
                    TutorScoutApi.shared.send(toSend: createOfferOrRequest, jsonUrl: jsonUrl, httpMethod: HTTPMethod.post, typeOfResponse: ResponseType.none) { (error, response, responseData) in
                        if(error != nil){
                            print("Debug-Description: "+error.debugDescription)
                        }
                        
                        if let resp = response{
                            print("[CreateOfferOrRequest] StatusCode: \(resp.statusCode)")
                            DispatchQueue.main.async {
                                self.form.setValues(["title": "", "description": "", "duration": Date(timeIntervalSince1970: 0)])
                                self.tableView.reloadData()
                            }
                            switch resp.statusCode {
                            case 200:
                                //Message
                                let alert = UIAlertController(title: "Erfolg", message: "Deine "+offorreq+" wurde erstellt.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                return
                            default:
                                //Message
                                let alert = UIAlertController(title: "Fehlgeschlagen", message: "Deine "+offorreq+" konnte nicht erstellt werden.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                return
                            }
                        }
                    }
        }
    }
}
