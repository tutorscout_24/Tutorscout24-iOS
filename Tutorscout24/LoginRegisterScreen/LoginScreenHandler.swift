//
//  LoginScreenHandler.swift
//  Tutorscout24
//
//  Created by Felix Schillinger on 18.11.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import Foundation

class LoginScreenHandler {
    
    static let shared: LoginScreenHandler = LoginScreenHandler()
    private init() {}
    
    //handles login for controller
    func handleLogin(username: String, password: String, loginViewController: LoginViewController){
        checkAuth(username: username, password: password, completion: { (error, success)  in
            if error {//displays message if no internet connection
                ConnectionError.shared.showConnectionError(controller: loginViewController)
            }else{//checks authenication
                if(success){
                    let ad = UIApplication.shared.delegate as! AppDelegate
                    if let window = ad.window {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        if let tabBarController = storyboard.instantiateViewController(withIdentifier: "root") as? TabBarViewController{
                            loginViewController.present(tabBarController, animated: true, completion: {
                                window.rootViewController = tabBarController
                            })
                        }
                    }
                }else{
                    print("showed infomessage")
                    loginViewController.showAlert(title: "Inkorrekte Anmeldedaten", message: "Dein eingegebener Username oder das Passwort ist falsch!", buttonText: "OK")
                }
            }
        })
    }
    
    //checks if user can authenticate
    private func checkAuth(username: String, password: String, completion: @escaping (_ error: Bool,_ success: Bool) -> ()){
        let username = username
        let password = password
        
        let authentication = Authentication(userName: username, password: password)
        let checkAuth = CheckAuthentication(authentication: authentication)
        
        //connect to server
        TutorScoutApi.shared.send(toSend: checkAuth, jsonUrl: JsonUrl.checkAuthentication, httpMethod: HTTPMethod.post, typeOfResponse: ResponseType.none) { (error, response, nil) in
            if error != nil{
                print(error.debugDescription)
                completion(true, false)
            }
            
            if let resp = response{
                switch resp.statusCode {
                case 200:
                    print("saved credentials")
                    //add credentials to keychain
                    let keychain = KeychainSwift()
                    keychain.set(username, forKey: "username")
                    keychain.set(password, forKey: "password")
                    //Dispatchqueue for running in main thread
                    DispatchQueue.main.async {
                        completion(false, true)
                    }
                    return
                case 400, 403:
                    DispatchQueue.main.async {
                        completion(false, false)
                    }
                    return
                default:
                    print("Another statuscode: \(resp.statusCode)")
                    DispatchQueue.main.async {
                        completion(false, false)
                    }
                }
            }
        }
    }
    
    //handles signup for controller
    func handleSignUp(createUser: Create, signUpViewController: SignupViewController){
        self.createUser(createUser: createUser) { (error, success, statusCode) in
            if error {//shows info to user if no internet connection
                ConnectionError.shared.showConnectionError(controller: signUpViewController)
            }else{
                if(success){
                    let ad = UIApplication.shared.delegate as! AppDelegate
                    if let window = ad.window {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        if let tabBarController = storyboard.instantiateViewController(withIdentifier: "root") as? TabBarViewController{
                            signUpViewController.present(tabBarController, animated: true, completion: {
                                window.rootViewController = tabBarController
                            })
                        }
                    }
                }else{
                    //check what kind field is wrong
                    switch statusCode{
                    case 400:
                        signUpViewController.showAlert(title: "Schwaches Passwort", message: "Das Passwort muss min. ein Klein- und Großbuchstaben, min. ein Sonderzeichen, min. eine Zahl und min. 8 Zeichen lang sein. ", buttonText: "OK")
                    case 500:
                        signUpViewController.showAlert(title: "User Name vergeben", message: "Dein gewählter User Name ist schon vergeben. Bitte wähle einen anderen", buttonText: "OK")
                    default:
                        signUpViewController.showAlert(title: "Ooops..", message: "Das sollte eigentlich nicht passieren. Probiere es später nochmal erneut", buttonText: "OK")
                    }
                }
                
            }
        }
    }
    
    
    private func createUser(createUser: Create, completion: @escaping (_ error: Bool, _ success: Bool, _ statusCode: Int) -> ()){
        //get password and username for saving it in keychain if successfully signed up
        let username = createUser.userName
        let password = createUser.password
        
        //connection to server
        TutorScoutApi.shared.send(toSend: createUser, jsonUrl: JsonUrl.createUser, httpMethod: HTTPMethod.post, typeOfResponse: ResponseType.none) { (error, response, nil) in
            if error != nil {
                print(error.debugDescription)
                completion(true, false, 0)
            }
            
            if let resp = response{
                switch resp.statusCode{
                case 200:
                    print("saved credentials")
                    //add credentials to keychain
                    let keychain = KeychainSwift()
                    keychain.set(username, forKey: "username")
                    keychain.set(password, forKey: "password")
                    print("created user successfully")
                    DispatchQueue.main.async {
                        completion(false, true, 200)
                    }
                    return
                case 400:
                    print("Password does not match regEx")
                    DispatchQueue.main.async {
                        completion(false, false, 400)
                    }
                    return
                case 500:
                    print("Username already in use")
                    DispatchQueue.main.async {
                        completion(false, false, 500)
                    }
                    return
                default:
                    print("Another statucode: \(resp.statusCode)")
                    DispatchQueue.main.async {
                        completion(false, false, resp.statusCode)
                    }
                }
            }
        }
    }
    
}
