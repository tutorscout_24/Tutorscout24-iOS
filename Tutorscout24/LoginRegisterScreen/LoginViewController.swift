//
//  LoginViewController.swift
//  Tutorscout24
//
//  Created by Felix Schillinger on 16.11.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var forgotButton: UIButton!
    @IBOutlet weak var usernameField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordField: SkyFloatingLabelTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addShadow()
        addRoundCornersToView(roundness: 20)
        setOpacityView(alpha: 0.7)
        setUpTextFields()
        setUpButtons(roundness: 10)
        
        self.view.bringSubview(toFront: loginView)
    }
    
    
    func setUpTextFields(){
        usernameField.placeholder = "Username"
        passwordField.placeholder = "Passwort"
    }
    
    func setUpButtons(roundness: Int){
        loginButton.layer.cornerRadius = CGFloat(roundness)
        signUpButton.layer.cornerRadius = CGFloat(roundness)
    }
    
    func addShadow(){
        loginView.layer.shadowRadius = 10
        loginView.layer.shadowOpacity = 1
        loginView.layer.shadowOffset = CGSize(width: 0, height: 5)
        loginView.layer.shadowColor = UIColor.black.cgColor
    }
    
    func addRoundCornersToView(roundness: Int){
        loginView.layer.cornerRadius = CGFloat(roundness)
    }
    
    func setOpacityView(alpha: Float){
        loginView.backgroundColor = UIColor(white: 1, alpha: CGFloat(alpha))
    }
    
    @IBAction func pressedLogin(_ sender: UIButton) {
        if let username = usernameField.text, let password = passwordField.text, !username.isEmpty, !password.isEmpty {
            LoginScreenHandler.shared.handleLogin(username: username, password: password, loginViewController: self)
        }else {
            showAlert(title: "Keine Anmeldedatan", message: "Trage dein Username und Passwort in die Felder ein.", buttonText: "OK")
        }
    }
    
    @IBAction func pressedSignup(_ sender: UIButton) {
        print("signupbutton pressed")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let signupVC = storyboard.instantiateViewController(withIdentifier: "signupVC")
        
        self.present(signupVC, animated: true, completion: nil)
    }
    
    
    @IBAction func forgotPassword(_ sender: UIButton) {
        showAlert(title: "Funktion nicht vorhanden", message: "Diese Funktion wird in der nächsten Version enthalten sein. Bis dahin musst du dir dein Passwort merken.", buttonText: "OK")
    }
    
    
    func showAlert(title: String, message: String, buttonText: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonText, style: .default, handler: nil))
        self.present(alert, animated: true, completion: {
            self.passwordField.text = ""
        })
    }

}
