//
//  LoginScreenHandler.swift
//  Tutorscout24
//
//  Created by Felix Schillinger on 18.11.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import Foundation

class APIHandler {
    
    static let shared: APIHandler = APIHandler()
    private init() {}
    
    
    func handleLogin(username: String, password: String, loginViewController: LoginViewController){
        checkAuth(username: username, password: password, completion: { (success) in
            if(success){
                let ad = UIApplication.shared.delegate as! AppDelegate
                if let window = ad.window {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    if let tabBarController = storyboard.instantiateViewController(withIdentifier: "root") as? TabBarViewController{
                        loginViewController.present(tabBarController, animated: true, completion: {
                            window.rootViewController = tabBarController
                        })
                    }
                }
            }else{
                print("showed infomessage")
                loginViewController.showAlert(title: "Wrong Password", message: "Username or password is wrong!", buttonText: "OK")
            }
        })
    }
    
    
    private func checkAuth(username: String, password: String, completion: @escaping (_ success: Bool) -> ()){
        let username = username
        let password = password
        
        let authentication = Authentication(userName: username, password: password)
        let checkAuth = CheckAuthentication(authentication: authentication)
        
        TutorScoutApi.shared.send(toSend: checkAuth, jsonUrl: JsonUrl.checkAuthentication, httpMethod: HTTPMethod.post, typeOfResponse: ResponseType.none) { (error, response, nil) in
            if error != nil{
                //TODO: show somthing went wrong with warning screen
            }
            
            if let resp = response{
                switch resp.statusCode {
                case 200:
                    print("saved credentials")
                    //add credentials to keychain
                    let keychain = KeychainSwift()
                    keychain.set(username, forKey: "username")
                    keychain.set(password, forKey: "password")
                    //Dispatchqueue for running in main thread
                    DispatchQueue.main.async {
                        completion(true)
                    }
                    return
                case 400, 403:
                    DispatchQueue.main.async {
                        completion(false)
                    }
                    return
                default:
                    print("Another statuscode: \(resp.statusCode)")
                    DispatchQueue.main.async {
                        completion(false)
                    }
                }
            }
        }
    }
    
    
}
