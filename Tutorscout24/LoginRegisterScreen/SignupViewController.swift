//
//  SignupViewController.swift
//  Tutorscout24
//
//  Created by Felix Schillinger on 17.11.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController, UIScrollViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    
    
    var signUpView: SignupView?
    let offset: Int = 20
    var backbutton: UIButton!
    var signupButton: UIButton!
    var maleButton: UIButton!
    var femaleButton: UIButton!
    var textFields: [SkyFloatingLabelTextField]!
    var textFieldsStrings: [String] = [String]()
    var scrollViewDidEndScrolling: Bool = false
    var gender: String!
    var genderButtonWasPressed: Bool = false
    var birthDatePicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showAlert(title: "Das ist ein Test", message: "Test, Test", buttonText: "OK")
        
        //check if xib exist
        if let signUpView = getSignUpView(xibName: "SignupView") as? SignupView{
            //setting up the view
            self.signUpView = signUpView
            setupSignUpView(alpha: 0.7)
            setupScrollView(width: scrollView.frame.width, frameHeight: self.view.bounds.height, contentHeight: self.signUpView!.frame.height + CGFloat(2*offset))
            loadViewInScrollView(signUpView: self.signUpView!)
            addRoundCornersToView(roundness: 20)
            addShadow()
            
            //get components from view
            backbutton = signUpView.backButton
            signupButton = signUpView.signupButton
            maleButton = signUpView.maleButton
            femaleButton = signUpView.femaleButton
            signUpView.configureSignupButton(roundness: 10)
            
            //add functionality to buttons
            backbutton.addTarget(self, action: #selector(dismissViewController), for: .touchUpInside)
            signupButton.addTarget(self, action: #selector(signup), for: .touchUpInside)
            maleButton.addTarget(self, action: #selector(setMale), for: .touchUpInside)
            femaleButton.addTarget(self, action: #selector(setFemale), for: .touchUpInside)
            
            signUpView.configureTextFields(viewController: self)
            textFields = signUpView.textFields
            birthDatePicker = signUpView.birthdatePicker
            
        }
        
        addReturnByRightSwipe()
    }
    
    //loads the xib file in controller
    func loadViewInScrollView(signUpView: UIView){
        scrollView.addSubview(signUpView)
        signUpView.center.x = scrollView.center.x
        signUpView.frame.origin.y = CGFloat(offset)
    }
    
    //set correct size of view
    func setupSignUpView(alpha: Float){
        signUpView?.frame.size.width = 300
        signUpView?.backgroundColor = UIColor(white: 1, alpha: CGFloat(alpha))
    }
    
    //set correct size of scrollview
    func setupScrollView(width: CGFloat, frameHeight: CGFloat, contentHeight: CGFloat) {
        scrollView.isScrollEnabled = true
        scrollView.isPagingEnabled = false
        scrollView.contentSize = CGSize(width: width, height: contentHeight)
        scrollView.frame.size.width = width
        scrollView.frame.size.height = frameHeight
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator =  true
        scrollView.delegate = self
    }
    
    
    func getSignUpView(xibName: String) -> UIView? {
        return Bundle.main.loadNibNamed(xibName, owner: self, options: nil)?.first as? UIView
    }
    
    func addRoundCornersToView(roundness: Int){
        signUpView?.layer.cornerRadius = CGFloat(roundness)
    }
    
    func addShadow(){
        signUpView?.layer.shadowRadius = 10
        signUpView?.layer.shadowOpacity = 1
        signUpView?.layer.shadowOffset = CGSize(width: 0, height: 5)
        signUpView?.layer.shadowColor = UIColor.black.cgColor
    }
    
    //get back with right swipe
    func addReturnByRightSwipe(){
        let rightSwipe: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(dismissViewController))
        rightSwipe.direction = .right
        self.view.addGestureRecognizer(rightSwipe)
    }
    
    @objc func dismissViewController(){
        self.dismiss(animated: true, completion: nil)
    }
    
    //gets called if signup button gets tapped
    @objc func signup(){
        //clear array
        textFieldsStrings.removeAll()
        
        for textfield in textFields {
            if let text = textfield.text, !text.isEmpty{
                textFieldsStrings.append(text)
            }else{
                showAlert(title: "Fehlende Felder", message: "Nicht alle Felder sind ausgefüllt!", buttonText: "OK")
                return
            }
        }
        
        //check if the password equals the repeated one
        if(textFieldsStrings[1] != textFieldsStrings[2]){
            showAlert(title: "Passwort stimmt nicht überein", message: "Dein angegebenes Passwort stimmt nicht mit dem wiederholten Passwort überein", buttonText: "OK")
            return
        }
        
        if(!genderButtonWasPressed){
            showAlert(title: "Kein Geschlecht?", message: "Bitte wähle dein biologisches Geschlecht.", buttonText: "OK")
            return
        }
        
        //get birthdate from date picker
        let birthdate = getBithdate(picker: birthDatePicker)

        let newUser = Create(userName: textFieldsStrings[0], password: textFieldsStrings[1], firstName: textFieldsStrings[3], lastName: textFieldsStrings[4], birthdate: birthdate, gender: gender, email: textFieldsStrings[5], note: textFieldsStrings[6], placeOfResidence: textFieldsStrings[7], maxGraduation: textFieldsStrings[8])

        LoginScreenHandler.shared.handleSignUp(createUser: newUser, signUpViewController: self)
    }
    
    @objc func setMale(){
        gender = "male"
        genderButtonWasPressed = true
    }
    
    @objc func setFemale(){
        gender = "female"
        genderButtonWasPressed = true
    }
    
    // converts the date from date bicker in the correct format
    func getBithdate(picker: UIDatePicker) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd"
        let selectedDate = dateFormatter.string(from: picker.date)
        return selectedDate
    }
    
    
    func showAlert(title: String, message: String, buttonText: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonText, style: .default, handler: nil))
        self.present(alert, animated: true, completion: {
            self.signUpView?.password.text = ""
            self.signUpView?.passwordRepeat.text = ""
        })
    }

}
