//
//  SignupView.swift
//  Tutorscout24
//
//  Created by Felix Schillinger on 18.11.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import UIKit


class SignupView: UIView {

    @IBOutlet weak var username: SkyFloatingLabelTextField!
    @IBOutlet weak var password: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordRepeat: SkyFloatingLabelTextField!
    @IBOutlet weak var firstName: SkyFloatingLabelTextField!
    @IBOutlet weak var lastName: SkyFloatingLabelTextField!
    @IBOutlet weak var email: SkyFloatingLabelTextField!
    @IBOutlet weak var note: SkyFloatingLabelTextField!
    @IBOutlet weak var placeOfResidence: SkyFloatingLabelTextField!
    @IBOutlet weak var maxGraduation: SkyFloatingLabelTextField!
    @IBOutlet weak var birthdatePicker: UIDatePicker!
    
    var textFields = [SkyFloatingLabelTextField]()
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    
    
    //configures the textfields from xib file
    func configureTextFields(viewController: UITextFieldDelegate){
        textFields.append(contentsOf: [username, password, passwordRepeat,firstName, lastName, email, note, placeOfResidence, maxGraduation])
        
        for field in textFields {
            field.delegate = viewController
        }
        
        birthdatePicker.maximumDate = Date()
        
        username.placeholder = "Username"
        password.placeholder = "Passwort"
        passwordRepeat.placeholder = "Passwort Wiederholen"
        passwordRepeat.title = "Wiederholtes Passwort"
        firstName.placeholder = "Vorname"
        lastName.placeholder = "Nachname"
        email.placeholder = "Email@example.com"
        email.title = "Email"
        note.placeholder = "Kleine Notiz..."
        note.title = "Notiz"
        placeOfResidence.placeholder = "Wohnort"
        maxGraduation.placeholder = "Höchster Abschluss"
        
        
    }
    
    func configureSignupButton(roundness: Int){
        signupButton.layer.cornerRadius = CGFloat(roundness)
    }

}
