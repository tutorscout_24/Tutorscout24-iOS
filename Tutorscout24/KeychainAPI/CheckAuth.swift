//
//  CheckAuth.swift
//  Tutorscout24
//
//  Created by Felix Schillinger on 17.11.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import Foundation

class AuthCheck{
    private let keyUsername: String = "username"
    private let keyPassword: String = "password"
    
    func isAlreadyAuth() -> Bool{
        var isAuth: Bool = false
        let group = DispatchGroup()
        let keychain = KeychainSwift()
        
        var checkAuth: CheckAuthentication!
        
        if let username = keychain.get(keyUsername), let password = keychain.get(keyPassword){
            print("username \(username), password \(password)")
            let authentication = Authentication(userName: username, password: password)
            checkAuth = CheckAuthentication(authentication: authentication)
        }else{
            return false
        }
        
        group.enter()
        
        TutorScoutApi.shared.send(toSend: checkAuth, jsonUrl: JsonUrl.checkAuthentication, httpMethod: HTTPMethod.post, typeOfResponse: ResponseType.none) { (error, response, nil) in
            if error != nil{
                print(error.debugDescription)
                group.leave()
            }else if let response = response{
                switch response.statusCode{
                case 200:
                    isAuth = true
                    group.leave()
                default:
                    group.leave()
                }
            }
        }
        
        group.wait()
        
        return isAuth
    }
    
}
