//
//  SingeltonManager.swift
//  Tutorscout24
//
//  Created by Jan Becker on 13.12.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

class GlobalVariables {
    
    public var events: EventManager = EventManager()
    private var mode = true;//True Schüler; False Lehrer
    private var messagesLoaded = false;
    
    //Returns actual mode (student/tutor)
    public func getMode() -> Bool {
        return mode;
    }
    
    //Switch mode (student/tutor)
    public func changeMode() {
        mode = !mode;
    }
    
    //Returns true if chat messages already loaded once
    public func getMessagesLoaded() -> Bool {
        return messagesLoaded
    }
    
    //Call if chat is loaded
    public func setMessagesLoaded() {
        messagesLoaded = true;
    }
    
    //Global instance
    class var sharedManager: GlobalVariables {
        struct Static {
            static let instance = GlobalVariables()
        }
        return Static.instance
    }
}
