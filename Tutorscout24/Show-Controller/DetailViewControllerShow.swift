//
//  DetailViewController.swift
//  Tutorscout24
//
//  Created by Felix Schillinger on 26.11.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import UIKit

class DetailViewControllerShow: UIViewController {

    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var messageButton: UIButton!
    
    var offerOrRequestRepsonse: GetOfferOrRequestsResponse!
    var dateString: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupChangeMode()
        convertTimeStampToNSDate()
        setUpLabelsAndButtons(roundnessButtons: 10, alpha: 0.5)
    }
    
    //converts the timestamp to a more user friendly time stamp
    func convertTimeStampToNSDate(){
        let dateFormatter = DateFormatter()
        let calendar = Calendar.current
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let dateString = offerOrRequestRepsonse.creationDate
        if let datePublished = dateFormatter.date(from: dateString) {
            
            let day = String(format: "%02d", calendar.component(.day, from: datePublished))
            let month = String(format: "%02d", calendar.component(.month, from: datePublished))
            let year = String(format: "%04d", calendar.component(.year, from: datePublished))
            let hour = String(format: "%02d", calendar.component(.hour, from: datePublished))
            let minute = String(format: "%02d", calendar.component(.minute, from: datePublished))
            print("\(day).\(month).\(year) at \(hour):\(minute)")
            self.dateString = day + "." + month + "." + year + " at " + hour + ":" + minute
        }
        
    }
    
    //sets all labels and buttons up
    func setUpLabelsAndButtons(roundnessButtons: Int, alpha: Float){
        messageButton.layer.cornerRadius = CGFloat(roundnessButtons)
        subjectLabel.lineBreakMode = .byWordWrapping
        subjectLabel.numberOfLines = 0
        subjectLabel.backgroundColor = UIColor.white.withAlphaComponent(CGFloat(alpha))
        subjectLabel.layer.masksToBounds = true
        subjectLabel.layer.cornerRadius = CGFloat(roundnessButtons)
        
        descriptionLabel.lineBreakMode = .byWordWrapping
        descriptionLabel.numberOfLines = 0
        descriptionLabel.backgroundColor = UIColor.white.withAlphaComponent(CGFloat(alpha))
        descriptionLabel.layer.masksToBounds = true
        descriptionLabel.layer.cornerRadius = CGFloat(roundnessButtons)
        
        setLableAndButtonTexts()
        
        
        
        
    }
    
    //writes content into labels
    func setLableAndButtonTexts(){
        subjectLabel.text = offerOrRequestRepsonse.subject
        descriptionLabel.text = offerOrRequestRepsonse.text
        authorLabel.text = "Autor: " + offerOrRequestRepsonse.userName
        dateLabel.text = self.dateString
        messageButton.setTitle("Kontaktiere " + offerOrRequestRepsonse.userName, for: .normal)
        
        let color = NavigationBarItems.uicolorFromHex(rgbValue: GlobalVariables.sharedManager.getMode()  ? 0xdb2e02 : 0x2980b9)
        messageButton.backgroundColor = color
    }
    
    //switches to the chat if user tappped message button
    @IBAction func messageButtonTapped(_ sender: UIButton) {
        if !GlobalVariables.sharedManager.getMessagesLoaded() {
            GlobalVariables.sharedManager.setMessagesLoaded()
            let chatController = self.storyboard!.instantiateViewController(withIdentifier: "ChatController") as! ChatController
            chatController.loadAllChatsFromServer()
        }
        
        let contact: Contact = Contact()
        contact.identifier = offerOrRequestRepsonse.userName
        contact.name = offerOrRequestRepsonse.userName
        
        let chat: Chat = Chat()
        chat.contact = contact
        
        let message: Message = Message()
        message.identifier = "Auto"
        message.text = "Wie kann ich dir weiterhelfen?"
        message.sender = .Someone
        message.status = .Received
        message.chatId = chat.identifier
        LocalStorage.sharedInstance.storeMessage(message: message)
        
        chat.lastMessage = message
        chat.numberOfUnreadMessages = 0
        
        let messageController = self.storyboard!.instantiateViewController(withIdentifier: "Message") as! MessageController
        messageController.chat = chat
        
        self.navigationController!.pushViewController(messageController, animated:true)
    }
    
    func setupChangeMode(){
        GlobalVariables.sharedManager.events.listenTo(eventName: "mc") { (information: Any?) in
            self.changeMode()
        }
    }
    
    //change color of button if mode changes
    func changeMode(){
        let color = NavigationBarItems.uicolorFromHex(rgbValue: GlobalVariables.sharedManager.getMode()  ? 0xdb2e02 : 0x0032E9)
        
        messageButton.backgroundColor = color
    }
    
}
