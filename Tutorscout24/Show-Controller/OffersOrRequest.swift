//
//  OffersOrRequest.swift
//  Tutorscout24
//
//  Created by Felix Schillinger on 13.12.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import Foundation

//class that provieds information gloabally
class OffersOrRequests{
    static let shared: OffersOrRequests = OffersOrRequests()
    private init() {}
    
    private var offersOrRequests: [GetOfferOrRequestsResponse] = [GetOfferOrRequestsResponse]()
    private var latitude: Double = 0
    private var longitude: Double = 0
    private var mapControllerInstance: MapViewControllerShow!
    private var tableControllerInstace: TableViewControllerShow!
    private var rangeKm: Int = 25
    private var useCoreLocation = false
    
    
    func setUseCoreLocation(useCoreLocation: Bool){
        self.useCoreLocation = useCoreLocation
    }
    
    func getUseCoreLocation() -> Bool{
        return self.useCoreLocation
    }
    
    func appendOffersOrRequests(offersOrRequest: [GetOfferOrRequestsResponse]){
        self.offersOrRequests.append(contentsOf: offersOrRequest)
    }
    
    func getRangeKm() -> Int{
        return rangeKm
    }
    
    func getOffersOrRequests() -> [GetOfferOrRequestsResponse]{
        return self.offersOrRequests
    }
    
    func getOfferOrRequest(atIndex: Int) -> GetOfferOrRequestsResponse{
        return self.offersOrRequests[atIndex]
    }
    
    func clearOffersOrRequests(){
        offersOrRequests.removeAll()
    }
    
    func setCoordiantes(latitude: Double, longitude: Double){
        self.latitude = latitude
        self.longitude = longitude
    }
    
    func getLatitude() -> Double{
        return self.latitude
    }
    
    func getLongitutde() -> Double{
        return self.longitude
    }
    
    func setMapControllerInstance(mapController: MapViewControllerShow){
        self.mapControllerInstance = mapController
    }
    
    func setTableControllerInstance(tableController: TableViewControllerShow){
        self.tableControllerInstace = tableController
    }
    
    func getMapController() -> MapViewControllerShow{
        return self.mapControllerInstance
    }
    
    func getTableController() -> TableViewControllerShow{
        return self.tableControllerInstace
    }
    
    func resetData(){
        offersOrRequests.removeAll()
        latitude = 0
        longitude = 0
        mapControllerInstance = nil
        tableControllerInstace = nil
        useCoreLocation = false
    }
}
