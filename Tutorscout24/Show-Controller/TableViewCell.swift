//
//  TableViewCell.swift
//  Tutorscout24
//
//  Created by Felix Schillinger on 25.11.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import UIKit

//the layout for the cell that is used for the tableview
class TableViewCell: UITableViewCell {

    @IBOutlet weak var subjectLable: UILabel!
    @IBOutlet weak var descriptionLable: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        subjectLable.lineBreakMode = .byWordWrapping
        subjectLable.numberOfLines = 0
        
        descriptionLable.lineBreakMode = .byTruncatingTail
        descriptionLable.numberOfLines = 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
