//
//  MapViewControllerShow.swift
//  Tutorscout24
//
//  Created by Felix Schillinger on 25.11.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import UIKit
import MapKit

class MapViewControllerShow: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var leftEdge: UIView!
    
    let manager = CLLocationManager()
    var coordinates: [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
    var firstLoad: Bool = true
    var selectedAnnotation: CustomPointAnnotation!
    var currentCity: String?
    var currentAddress: String?
    var alreadyHaveLocation: Bool = false
    var spanDelta: Double!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spanDelta = (1/111) * Double(OffersOrRequests.shared.getRangeKm())
        checkIfLocationServiceIsEnabled()
        OffersOrRequests.shared.setMapControllerInstance(mapController: self)
        self.view.bringSubview(toFront: leftEdge)
        setupLocationManager()
        addAnnotations()
        mapView.delegate = self
        // set observer for UIApplicationWillEnterForeground
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: .UIApplicationWillEnterForeground, object: nil)
    }
    
    //checks if user really changed settings
    @objc func willEnterForeground(){
        if !alreadyHaveLocation{
            checkIfLocationServiceIsEnabled()
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print("Memory Warning in 'MapViewControllerShow!'")
    }
    
    //when app starts this method checks if user uses GPS or manually setted location
    func checkIfLocationServiceIsEnabled(){
        if CLLocationManager.locationServicesEnabled(){//GPS is enabled
            switch CLLocationManager.authorizationStatus(){
            case .restricted, .notDetermined, .denied: //not allowed to use GPS (yet)
                OffersOrRequests.shared.setUseCoreLocation(useCoreLocation: false)
                self.showAlert(title: "Location", message: "Die App benötigt deinen Standort")
                break
            default: // allowed to use GPS
                print("Everything is okay with location settings")
                OffersOrRequests.shared.setUseCoreLocation(useCoreLocation: true)
                refreshData()
                break
            }
        }else{//GPS is not enabled
            OffersOrRequests.shared.setUseCoreLocation(useCoreLocation: false)
            self.showAlert(title: "Location", message: "Die App benötigt deinen Standort")
        }
    }
    
    //updates users position if it changes
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        
        let span: MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: spanDelta, longitudeDelta: spanDelta)
        let currentLocation: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region: MKCoordinateRegion = MKCoordinateRegion(center: currentLocation, span: span)
        OffersOrRequests.shared.setCoordiantes(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        
        //reverse geocoding for current location
        CLGeocoder().reverseGeocodeLocation(location) { (placemarks, error) in
            if(error != nil){
                print("error during reverse geocoding: \(error.debugDescription)")
            }else if let place = placemarks?[0]{
                if let city = place.locality, let street = place.thoroughfare, let streetNumber = place.subThoroughfare{
                    print("city: \(city), street: \(street) \(streetNumber)")
                    CurrentLocation.shared.setCityAndStreet(city: city, street: street + " " + streetNumber)
                }
            }
        }
        //after first time fetched coords, the TVC gets the data
        if(firstLoad){
            firstLoad = false
            OffersOrRequests.shared.getTableController().refreshDataFromServer()
        }
        mapView.setRegion(region, animated: true)
        mapView.showsUserLocation = false
    }
    
    //colors the pin annotation on the map
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var view = mapView.dequeueReusableAnnotationView(withIdentifier: "annotationIdentifier")
        let customAnnotation = annotation as! CustomPointAnnotation
        if customAnnotation.identifier == 0{ //check if it is the users annotation
            let userLocationAnnotation = MKPinAnnotationView(annotation: customAnnotation, reuseIdentifier: "annotationIdentifier")
            userLocationAnnotation.pinTintColor = UIColor.blue
            userLocationAnnotation.canShowCallout = false
            return userLocationAnnotation
        }else{ //if the annotation doesn't exist
            view = MKPinAnnotationView(annotation: customAnnotation, reuseIdentifier: "annotationIdentifier")
            view?.canShowCallout = true
            view?.rightCalloutAccessoryView = UIButton(type: .infoLight)
        }
        return view
    }
    
    //performs segue if detail button is clicked
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if control == view.rightCalloutAccessoryView {
            selectedAnnotation = view.annotation as? CustomPointAnnotation
            performSegue(withIdentifier: "showDetailSegue", sender: self)
        }
    }
    
    //adds the feteched request/offers to the map
    func addAnnotations(){
        //remove old annotations after every request
        mapView.removeAnnotations(self.mapView.annotations)
        let ofOrRes = OffersOrRequests.shared.getOffersOrRequests()
        //add user location as an annoation
        let userLocation = CustomPointAnnotation(identifier: 0)
        let userCoordinate = CLLocationCoordinate2D(latitude: OffersOrRequests.shared.getLatitude(), longitude: OffersOrRequests.shared.getLongitutde())
        userLocation.coordinate = userCoordinate
        userLocation.title = "You"
        mapView.addAnnotation(userLocation)
        //center map to user location
        let span: MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: spanDelta, longitudeDelta: spanDelta)
        let currentLocation = CLLocationCoordinate2D(latitude: OffersOrRequests.shared.getLatitude(), longitude: OffersOrRequests.shared.getLongitutde())
        let region: MKCoordinateRegion = MKCoordinateRegion(center: currentLocation, span: span)
        mapView.setRegion(region, animated: true)
        
        for (index, ofOrRe) in ofOrRes.enumerated() {
            let annotation = CustomPointAnnotation(identifier: index + 1)
            let coordinate = CLLocationCoordinate2D(latitude: Double(ofOrRe.latitude), longitude: Double(ofOrRe.longitude))
            let title = ofOrRe.subject
                
            annotation.coordinate = coordinate
            annotation.title = title
            
            mapView.addAnnotation(annotation)
        }
    }
    
    //gives data to DetailViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detailViewController = segue.destination as! DetailViewControllerShow
        detailViewController.offerOrRequestRepsonse = OffersOrRequests.shared.getOfferOrRequest(atIndex: selectedAnnotation.identifier - 1)
    }
    
    func setupLocationManager(){
        manager.delegate = self
        manager.requestAlwaysAuthorization()
        manager.startMonitoringSignificantLocationChanges()
        manager.pausesLocationUpdatesAutomatically = true
        mapView.isScrollEnabled = true
        mapView.isZoomEnabled = true
        mapView.isRotateEnabled = false
        mapView.isPitchEnabled = false
    }
    
    
    func showAlert(title: String, message: String){
        //Create the alert controller.
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        //Add the text fields
        alert.addTextField { (cityTextField) in
            cityTextField.placeholder = "Stadt"
        }
        
        alert.addTextField { (addressTextField) in
            addressTextField.placeholder = "Straße"
        }
        
        alert.addAction(UIAlertAction(title: "Einstellungen", style: .default, handler: {(_) in
            if let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION/com.dhbw-stuttgart.hb.Tutorscout24") {
                // If general location settings are disabled then open general location settings
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }))
        //Grab the value from the text field, and print it when the user clicks Übernehmen
        alert.addAction(UIAlertAction(title: "Übernehmen", style: .default, handler: { [weak alert] (_) in
            if let city = alert?.textFields?[0].text, let address = alert?.textFields?[1].text{
                if (city.isEmpty || address.isEmpty){
                    self.showAlert(title: "Fehlende Felder!", message: "Ort und Adresse müssen angegeben werden")
                }else{ // if fields are filled, check if place exist
                    CLGeocoder().geocodeAddressString(city + " " + address, completionHandler: { (placemarks, error) in
                        if error != nil{ // field does not exist
                            self.showAlert(title: "Inkorrekte Adresse!", message: "Die Angegeben Adresse scheint nicht zu existieren")
                        }else if let place = placemarks?[0]{ // field exist
                            print("location of \(place.locality!) is \(place.location!)")
                            if let location = place.location, let city = place.locality, let street = place.thoroughfare{
                                //safe location so it's available globally
                                if let streetNumber = place.subThoroughfare{
                                    CurrentLocation.shared.setCityAndStreet(city: city, street: street + " " + streetNumber)
                                }else {
                                    CurrentLocation.shared.setCityAndStreet(city: city, street: street)
                                }
                                OffersOrRequests.shared.setCoordiantes(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                                self.refreshData()
                            }
                        }
                    })
                }
            }
        }))
        //Presents the alert.
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func refreshData(){
        //refresh tableview
        OffersOrRequests.shared.getTableController().refreshDataFromServer()
        //location has been fetched
        self.alreadyHaveLocation = true
    }
}
