//
//  CustomPointAnnotation.swift
//  Tutorscout24
//
//  Created by Felix Schillinger on 14.12.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import UIKit
import MapKit

//extends MKPointAnnotation so it has an identifier
class CustomPointAnnotation: MKPointAnnotation {
    var identifier: Int!
    
    init(identifier: Int) {
        super.init()
        self.identifier = identifier
    }
}
