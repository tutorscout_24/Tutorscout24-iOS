//
//  CurrentLocation.swift
//  Tutorscout24
//
//  Created by Felix Schillinger on 26.12.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import Foundation

//class for saving the entered street and city from the user
class CurrentLocation {
    static let shared: CurrentLocation = CurrentLocation()
    private init() {}
    
    private var city = ""
    private var street = ""
    
    func setCityAndStreet(city: String, street: String){
        self.city = city
        self.street = street
    }
    
    func getCity() -> String {
        return self.city
    }
    
    func getStreet() -> String {
        return self.street
    }
    
}
