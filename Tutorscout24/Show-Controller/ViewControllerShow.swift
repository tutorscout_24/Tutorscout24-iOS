//
//  ViewControllerShow.swift
//  Tutorscout24
//
//  Created by Jan Becker on 19.10.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import UIKit

class ViewControllerShow: UIViewController, UIScrollViewDelegate{

    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var costumSegementedControl: UICostumSegmentedController!
    var enableScrollViewDidScrollFunc: Bool = true
    var viewDidLayoutSubviewsAlready = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NavigationBarItems.initializeNavigationBar(navItem: self.navigationItem, title: "Anzeigen")
    }
    
    //wait till layouts are set and place views in controller
    override func viewDidLayoutSubviews() {
        if(!viewDidLayoutSubviewsAlready){
            print("ViewDidLayoutSubviews")
            viewDidLayoutSubviewsAlready = true
            setupScrollView(width: self.view.frame.size.width, height: scrollView.frame.height)
            loadScrollViewViews(width: self.view.frame.size.width, height: scrollView.frame.height)
        }
        
    }
   
    //sets up the scrollview so content fits
    func setupScrollView(width: CGFloat, height: CGFloat) {
        scrollView.isScrollEnabled = true
        scrollView.isPagingEnabled = true
        scrollView.contentSize = CGSize(width: width * 2, height: height)
        scrollView.frame.size.width = width
        scrollView.frame.size.height = height
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator =  false
        scrollView.delegate = self
    }
    
    //loads the the views from other contoler in the the scrollview
    func loadScrollViewViews(width: CGFloat, height: CGFloat) {
        addTableViewController(width: width, height: height)
        addMapViewController(width: width, height: height)
    }
    
    //adds the TableViewsControllers view to the scroll view
    func addTableViewController(width: CGFloat, height: CGFloat){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let showTableViewController = storyboard.instantiateViewController(withIdentifier: "tableViewControllerShow")
        self.addChildViewController(showTableViewController)
        let showTableView = showTableViewController.view!
        
        scrollView.addSubview(showTableView)
        showTableView.frame.size.width = width
        showTableView.frame.size.height = height
        showTableView.frame.origin.x = 0
        showTableViewController.didMove(toParentViewController: self)
    }
    
    //adds the MapViewContolllers view to the scroll viwe
    func addMapViewController(width: CGFloat, height: CGFloat){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let showMapViewController = storyboard.instantiateViewController(withIdentifier: "showMapViewController")
        self.addChildViewController(showMapViewController)
        let showMapView = showMapViewController.view!
        
        scrollView.addSubview(showMapView)
        showMapView.frame.size.width = width
        showMapView.frame.size.height = height
        showMapView.frame.origin.x = width
        showMapViewController.didMove(toParentViewController: self)
    }
    
    //helper function that indicates if user tapped button or scroll through scroll view
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        enableScrollViewDidScrollFunc = true
    }
    
    //sets the current position of the index of the segmented control
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(enableScrollViewDidScrollFunc){
            costumSegementedControl.setCurrentPositionOfSelector(currentPosition: Float(scrollView.contentOffset.x))
        }
    }
    
    //let the little white bar moving during swiping
    @IBAction func viewChange(_ sender: UICostumSegmentedController) {
        enableScrollViewDidScrollFunc = false
        let page: CGFloat = CGFloat(sender.selectedSegmentIndex)
        var contentOffset: CGPoint = scrollView.contentOffset
        contentOffset.x = scrollView.frame.width * CGFloat(page)
        scrollView.setContentOffset(contentOffset, animated: true)
    }
    
}

