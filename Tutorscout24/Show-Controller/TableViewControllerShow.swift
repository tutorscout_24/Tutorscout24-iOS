//
//  TableViewControllerShow.swift
//  Tutorscout24
//
//  Created by Felix Schillinger on 25.11.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import UIKit

class TableViewControllerShow: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    
    let ofOrRe = OffersOrRequests.shared
    var rangeKm: Int!
    var currentIndex: Int = 0
    var lastOfferOrRequestResponseCountBeforeUpdate: Int = 0
    var firstLoad: Bool = true
    let rowOffset: Int = 10
    let rowLimit: Int = 10
    private var jsonURL: JsonUrl = JsonUrl.offers
    
    override func viewDidLoad() {
        super.viewDidLoad()
        rangeKm = OffersOrRequests.shared.getRangeKm()
        setupChangeMode()
        ofOrRe.setTableControllerInstance(tableController: self)
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Ziehen zum refreshen")
        refreshControl.addTarget(self, action: #selector(refreshDataFromServer), for: .valueChanged)
        
        tableView.addSubview(refreshControl)
        
        tableView.tableFooterView = UIView()
        
        loadOfferOrRequests(rowOffset: 0)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 80
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    //fills the tableview with content
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let numOfRows: Int = ofOrRe.getOffersOrRequests().count
        if (numOfRows > 0) {
            tableView.separatorStyle = .singleLine
            tableView.backgroundView = nil
        }
        else if(activityIndicator.isAnimating) {//no data is avaiable
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text = "Keine Daten verfügbar"
            noDataLabel.textColor = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
            tableView.separatorStyle = .none
        }
        return numOfRows
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        
        cell.subjectLable.text = ofOrRe.getOffersOrRequests()[indexPath.row].subject
        cell.descriptionLable.text = ofOrRe.getOffersOrRequests()[indexPath.row].text
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentIndex = indexPath.row
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "segue", sender: self)
    }
    
    //automatically reloads data from server if user scrolls to bottom
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = ofOrRe.getOffersOrRequests().count - 1
        if(indexPath.row == lastElement){
            print("reached last cell in section!")
            inserNewOffersOrRequests()
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detailViewController = segue.destination as! DetailViewControllerShow
        
        detailViewController.offerOrRequestRepsonse = self.ofOrRe.getOffersOrRequests()[currentIndex]
    }
    
    //loads offers/requests from server with given offset
    func loadOfferOrRequests(rowOffset: Int){
        let keychain = KeychainSwift()
        let username = keychain.get("username")!
        let password = keychain.get("password")!
        lastOfferOrRequestResponseCountBeforeUpdate = ofOrRe.getOffersOrRequests().count //safe current loaded offers/request
        let getOfferOrrequest = GetOfferOrRequests(latitude: ofOrRe.getLatitude(), longitude: ofOrRe.getLongitutde(), rangeKm: rangeKm, rowLimit: rowLimit, rowOffset: rowOffset, authentication: Authentication(userName: username, password: password))
        
        activityIndicator.startAnimating()
        
        //get data from server
        TutorScoutApi.shared.send(toSend: getOfferOrrequest, jsonUrl: jsonURL, httpMethod: HTTPMethod.post, typeOfResponse: ResponseType.getOfferOrRequestsResponse) { (error, response, responseData) in
            if(error != nil){
                print("some error occured: \(error.debugDescription)")
                DispatchQueue.main.async {
                    self.reloadDataAndEndLoadingAnimation()
                    self.refreshControl.endRefreshing()
                    ConnectionError.shared.showConnectionError(controller: self)
                }
            }
            
            if let response = response{
                print("status code: \(response.statusCode)")
                if(response.statusCode != 200){
                    self.activityIndicator.stopAnimating()
                }
            }
            
            if let offersResponse = responseData as? [GetOfferOrRequestsResponse] {
                print("offerresponse.count: \(offersResponse.count)")
                self.ofOrRe.appendOffersOrRequests(offersOrRequest: offersResponse)
                DispatchQueue.main.async {
                    self.reloadDataAndEndLoadingAnimation()
                }
            }
        }
        
        
    }
    
    //loads data in tableview (if there is data)
    func reloadDataAndEndLoadingAnimation(){
        if(!firstLoad){
            let dataToAddCount = ofOrRe.getOffersOrRequests().count - lastOfferOrRequestResponseCountBeforeUpdate
            if(dataToAddCount == 0){
                activityIndicator.stopAnimating()
                return
            }
            
            var indexPaths = [IndexPath]()
            for i in 0..<dataToAddCount {
                indexPaths.append(IndexPath(row: lastOfferOrRequestResponseCountBeforeUpdate + i, section: 0))
            }
            
            tableView.beginUpdates()
            tableView.insertRows(at: indexPaths, with: .bottom)
            tableView.endUpdates()
        }else{
            firstLoad = false
        }
        GlobalVariables.sharedManager.events.trigger(eventName: "showReady", information: "showView is Ready!")
        //add annotations on map after fetching requests
        ofOrRe.getMapController().addAnnotations()
        activityIndicator.stopAnimating()
        
    }
    
    func inserNewOffersOrRequests(){
        let currentOfferOrRequestCount = ofOrRe.getOffersOrRequests().count
        loadOfferOrRequests(rowOffset: currentOfferOrRequestCount)
    }
    
    //deletes all content in tableview and reload data from server
    @objc func refreshDataFromServer(){
        ofOrRe.clearOffersOrRequests()
        lastOfferOrRequestResponseCountBeforeUpdate = 0
        tableView.reloadData()
        loadOfferOrRequests(rowOffset: 0)
        refreshControl.endRefreshing()
    }
    
    func setupChangeMode(){
        GlobalVariables.sharedManager.events.listenTo(eventName: "mc") { (information: Any?) in
                self.changeMode(isTutor: !GlobalVariables.sharedManager.getMode())
        }
    }
    
    func changeMode(isTutor: Bool){
        print("is in changeMode function with isTutor: \(isTutor)")
        if(isTutor){
            jsonURL = .requests
        }else{
            jsonURL = .offers
        }
        refreshDataFromServer()
    }
    

}
