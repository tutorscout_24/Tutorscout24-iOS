//
//  UICostumSegmentedController.swift
//  Tutorscout24
//
//  Created by Felix Schillinger on 27.10.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import UIKit

@IBDesignable
class UICostumSegmentedController: UIControl {
    var buttons = [UIButton]()
    var selector: UIView!
    var widthOfFrame: CGFloat = 0 //for getting constraints
    var selectedSegmentIndex: Int = 0
    
    
    //all inspectables are visible in the inteface builder
    @IBInspectable
    var borderWidth: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable
    var borderColor: UIColor = UIColor.clear {
        didSet{
            self.layer.borderColor =  borderColor.cgColor
        }
    }
    
    @IBInspectable
    var roundCorners: Bool = false {
        didSet {
            if(roundCorners){
                self.layer.cornerRadius = frame.height/2
            }
        }
    }
    
    @IBInspectable
    var csvButtonTitles: String = ""{
        didSet {
            viewUpdate()
        }
    }
    
    @IBInspectable
    var textColor: UIColor = UIColor.lightGray{
        didSet {
            viewUpdate()
        }
    }
    
    @IBInspectable
    var selectorColor: UIColor = UIColor.darkGray{
        didSet {
            viewUpdate()
        }
    }
    
    @IBInspectable
    var selectorTextColor: UIColor = UIColor.white {
        didSet {
            viewUpdate()
        }
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //self.frame will be correct here
        widthOfFrame = self.frame.width
        viewUpdate()
        //addShadow
        GlobalVariables.sharedManager.events.listenTo(eventName: "mc") {
            self.backgroundColor = NavigationBarItems.uicolorFromHex(rgbValue: GlobalVariables.sharedManager.getMode()  ? 0xdb2e02 : 0x2980b9)
        }
    }
    
    //add a light shadow to the segmented conrol
    func addShadow(){
        layer.shadowRadius = 10
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 0, height: 10)
        layer.shadowColor = UIColor.black.cgColor
    }
    
    //updates the view if nesscessary
    func viewUpdate(){
        buttons.removeAll()
        self.subviews.forEach { (view) in
            view.removeFromSuperview()
        }
        
        //get button titles
        let buttonTitles = csvButtonTitles.components(separatedBy: ",")
        
        //create button for every title
        for buttonTitle in buttonTitles {
            let button = UIButton(type: .system)
            button.setTitleColor(textColor, for: .normal)
            button.setTitle(buttonTitle, for: .normal)
            button.titleLabel?.font = UIFont.init(name: "neuropol", size: 12)
            button.addTarget(self, action: #selector(buttonTapped(button:)), for: .touchUpInside)
            buttons.append(button)
        }
        
        //set the first button as the initial button
        buttons[0].setTitleColor(selectorTextColor, for: .normal)
        
        selector = UIView(frame: CGRect(x: 0, y: (self.frame.height - 3), width: widthOfFrame/CGFloat(buttons.count), height: 3))
        if(roundCorners){
            selector.layer.cornerRadius = selector.frame.height/2
        }
        selector.backgroundColor = selectorColor
        self.addSubview(selector)
        
        //add the buttons to the control
        let stackView = UIStackView(arrangedSubviews: buttons)
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        self.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        stackView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        stackView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
    }
    
    //indicates if user tapped a button and change the segment
    @objc func buttonTapped(button: UIButton){
        for (buttonIndex, btn) in buttons.enumerated(){
            btn.setTitleColor(textColor, for: .normal)
            if(btn == button){
                selectedSegmentIndex = buttonIndex
                let selectorStartPosition = widthOfFrame/CGFloat(buttons.count) * CGFloat(buttonIndex)
                UIView.animate(withDuration: 0.3, animations: {
                    self.selector.frame.origin.x = selectorStartPosition
                })
                
                btn.setTitleColor(selectorTextColor, for: .normal)
            }
        }
        
        sendActions(for: .valueChanged)
    }
    
    //sets the current position of the selector 
    func setCurrentPositionOfSelector(currentPosition: Float){
        selector.frame.origin.x = CGFloat(currentPosition)/CGFloat(buttons.count)
        if(Int(currentPosition) % Int(widthOfFrame) == 0){
            for btn in buttons{
                btn.setTitleColor(textColor, for: .normal)
            }
            buttons[Int(Int(currentPosition)/Int(widthOfFrame))].setTitleColor(selectorTextColor, for: .normal)
        }
    }
}
