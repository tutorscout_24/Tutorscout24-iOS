//
//  LocalStorage.swift
//  Tutorscout24 - Chat
//
//  Created by Jan Becker on 31.10.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import UIKit

class LocalStorage {
    private var mapChatToMessages:[String:[Message]] = [String:[Message]]()

    //Stores message in LocalStorage (Singleton Object)
    func storeMessage(message:Message) {
        self.storeMessages(messages: [message])
    }
    
    //Stores messages in LocalStorage (Singelton Object)
    func storeMessages(messages:[Message]) {
        if messages.count == 0 {
            return
        }
        
        let message = messages[0]
        let chatId = message.chatId
        
        var array = self.queryMessagesForChatId(chatId: chatId)
        if array != nil  {
            var newArray = messages
            var messageId = 0
            for message in messages {
                if array!.contains(message) {
                    newArray.remove(at: messageId)
                }
                else {
                    for m in array! { 
                        if m.identifier == "Auto" && m.identifier == message.identifier{
                            newArray.remove(at: messageId)
                            break
                        }
                    }
                }
                messageId = messageId+1
            }
            array!.append(contentsOf: newArray)
        }
        else {
            array = messages
        }
        
        self.mapChatToMessages[chatId] = array
    }
    
    //Clears chats
    func deleteAll() {
        self.mapChatToMessages.removeAll()
    }
    
    //Add message to specific chat
    func queryMessagesForChatId(chatId:String) -> [Message]?
    {
        return self.mapChatToMessages[chatId]
    }

    func storeChat(chat:Chat) {
        //TODO
    }
    
    func storeChats(chats:[Chat]) {
        //TODO
    }
    
    func storeContact(contact:Contact) {
        //TODO
    }
    
    func storeContacts(contacts:[Contact]) {
        //TODO
    }
    
    static let sharedInstance = LocalStorage()
    private init() {}
}
