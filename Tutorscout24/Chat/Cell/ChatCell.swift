//
//  ChatListCell.swift
//  Tutorscout24 - Chat
//
//  Created by Jan Becker on 31.10.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import UIKit

//Class for custom table Cell -> Chat
class ChatCell:UITableViewCell {
    @IBOutlet weak var nameLabel:UILabel!
    @IBOutlet weak var messageLabel:UILabel!
    @IBOutlet weak var timeLabel:UILabel!
    @IBOutlet weak var picture:UIImageView!
    @IBOutlet weak var notificationLabel:UILabel!
    
    override var imageView:UIImageView {
        set {
            
        }
        get {
            return self.picture
        }
    }
    
    var chat:Chat! {
        didSet {
            self.nameLabel.text = chat.contact.name
            self.messageLabel.text = chat.lastMessage.text
            self.updateTimeLabelWithDate(date: chat.lastMessage.date)
            self.updateUnreadMessagesIcon(numberOfUnreadMessages: chat.numberOfUnreadMessages)
        }
    }

    override func awakeFromNib () {
        self.picture.layer.cornerRadius = self.picture.frame.size.width/2
        self.picture.layer.masksToBounds = true
        self.notificationLabel.layer.cornerRadius = self.notificationLabel.frame.size.width/2
        self.notificationLabel.layer.masksToBounds = true
        self.nameLabel.text = ""
        self.messageLabel.text = ""
        self.timeLabel.text = ""
    }
    
    func updateTimeLabelWithDate(date:NSDate) {
        let df = DateFormatter()
        df.timeStyle = .short
        df.dateStyle = .none
        df.doesRelativeDateFormatting = false
        self.timeLabel.text = df.string(from: date as Date)
    }
    
    func updateUnreadMessagesIcon(numberOfUnreadMessages:Int)
    {
        self.notificationLabel.isHidden = numberOfUnreadMessages == 0
        self.notificationLabel.text = String(numberOfUnreadMessages)
    }
}
