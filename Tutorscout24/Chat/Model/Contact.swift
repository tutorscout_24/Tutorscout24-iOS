//
//  Contact.swift
//  Tutorscout24 - Chat
//
//  Created by Jan Becker on 31.10.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import UIKit

class Contact {
    var identifier:String = ""
    var name:String = ""
    var imageId:String = ""

    //Getting contact from dictionary
    func contactFromDictionary(dict:NSDictionary) -> Contact {
        let contact = Contact()
        contact.name = dict["name"] as! String
        contact.identifier = dict["id"] as! String
        contact.imageId = dict["imageId"] as! String
        return contact
    }
    
    //Returns true if contact has an image
    func hasImage() -> Bool {
        return self.imageId.characters.count > 0
    }
    
    //Save contact
    func save() {
        LocalStorage.sharedInstance.storeContact(contact: self)
    }
}
