//
//  Chat.swift
//  Tutorscout24 - Chat
//
//  Created by Jan Becker on 31.10.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import UIKit
import Foundation

class Chat {
    var contact:Contact!
    var numberOfUnreadMessages:Int = 0
    
    //Last Message from chat (Shown in chat controller)
    var lastMessage:Message! {
        willSet (val) {
            if self.lastMessage != nil {
                if self.lastMessage.date.earlierDate(val.date as Date) != self.lastMessage.date as Date {
                    self.lastMessage = val
                }
            }
        }
    }

    //ChatId
    var identifier:String {
        get {
            return self.contact.identifier
        }
    }

    //Saves chat in LocalStorage
    func save() {
        LocalStorage.sharedInstance.storeChat(chat: self)
    }
}
