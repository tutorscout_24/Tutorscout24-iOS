//
//  Message.swift
//  Tutorscout24 - Chat
//
//  Created by Jan Becker on 31.10.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import UIKit

//States for messages
public enum MessageStatus {
    case Sending
    case Sent
    case Received
    case Read
    case Failed
}

//Possible Senders for Messages
public enum MessageSender {
    case Myself
    case Someone
}

class Message:NSObject {
    var sender:MessageSender = .Myself
    var status:MessageStatus = .Sending
    var identifier:String = ""
    var chatId:String = ""
    var text:String = ""
    var date:NSDate = NSDate()
    var height:CGFloat = 44

    //Get message from dictionary
    func messageFromDictionary(dictionary:NSDictionary) -> Message {
        let message = Message()
        message.text = dictionary["text"] as! String
        message.identifier = dictionary["messageId"] as! String
        message.status = dictionary["status"] as! MessageStatus
        
        let dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS"
        
        //Date in UTC
        let inputTimeZone = NSTimeZone(abbreviation: "UTC")
        let inputDateFormatter = DateFormatter()
        inputDateFormatter.timeZone = inputTimeZone as TimeZone!
        inputDateFormatter.dateFormat = dateFormat
        let date = inputDateFormatter.date(from: dictionary["sent"] as! String)
        
        //Convert time in UTC to Local TimeZone
        let outputTimeZone = NSTimeZone.local
        let outputDateFormatter = DateFormatter()
        outputDateFormatter.timeZone = outputTimeZone
        outputDateFormatter.dateFormat = dateFormat
        let outputString = outputDateFormatter.string(from: date!)
        
        message.date = outputDateFormatter.date(from: outputString)! as NSDate
        
        return message
    }
    
    //Convert Server Response to message
    func messageFromServerResponse(sent: Bool, read: Bool, messageResponse: GetMessagesResponse) -> Message {
        let message = Message()
        message.text = messageResponse.text
        message.identifier = "\(messageResponse.messageId)"
        if sent == true {
            message.chatId = messageResponse.toUserId
            message.sender = .Myself
        }
        else {
            message.chatId = messageResponse.fromUserId
            message.sender = .Someone
        }
        message.status = .Received
        if read == true {message.status = .Read}
        
        let dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        //Date in UTC
        let inputTimeZone = NSTimeZone(abbreviation: "UTC")
        let inputDateFormatter = DateFormatter()
        inputDateFormatter.timeZone = inputTimeZone as TimeZone!
        inputDateFormatter.dateFormat = dateFormat
        let date = inputDateFormatter.date(from: messageResponse.datetime)
        
        //Convert time in UTC to Local TimeZone
        let outputTimeZone = NSTimeZone.local
        let outputDateFormatter = DateFormatter()
        outputDateFormatter.timeZone = outputTimeZone
        outputDateFormatter.dateFormat = dateFormat
        let outputString = outputDateFormatter.string(from: date!)
        
        message.date = outputDateFormatter.date(from: outputString)! as NSDate
        
        return message
    }
}
