//
//  MessageGateway.swift
//  Tutorscout24 - Chat
//
//  Created by Jan Becker on 31.10.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

@objc protocol MessageGatewayDelegate:NSObjectProtocol {
    func gatewayDidUpdateStatusForMessage(message:Message)
    func gatewayDidReceiveMessages(array:[Message])
}


class MessageGateway:NSObject {
    var delegate:MessageGatewayDelegate!
    var chat:Chat!
    var messagesToSend:NSMutableArray = NSMutableArray()

    //Reloads messages that already have been loaded
    func loadOldMessages() {
        let array = LocalStorage.sharedInstance.queryMessagesForChatId(chatId: self.chat.identifier)
        if array == nil {
            return
        }

        self.delegate?.gatewayDidReceiveMessages(array: array!)
 
        let unreadMessages = self.queryUnreadMessagesInArray(array: array!)
        self.updateStatusToReadInArray(unreadMessages: unreadMessages)
    }
    
    //Changes Status to Read from Messages
    func updateStatusToReadInArray(unreadMessages:[Message]) {
        var readIds = [String]()
        for message in unreadMessages {
            message.status = .Read
            readIds.append(message.identifier)
        }
        self.chat.numberOfUnreadMessages = 0
        self.sendReadStatusToMessages(messageIds: readIds)
    }
    
    //Returns unread messages
    func queryUnreadMessagesInArray(array:[Message]) -> [Message] {
        return array.filter({(message:Message) -> Bool in
            return message.status == .Received
        })
    }
    
    func news() {
        
    }
    func dismiss() {
        self.delegate = nil
    }
    
    //Function for changing the status of a message
    @objc func updateMessageStatus(message:Message) {
        if self.delegate != nil && self.delegate.responds(to: #selector(MessageGatewayDelegate.gatewayDidUpdateStatusForMessage(message:))) {
            self.delegate!.gatewayDidUpdateStatusForMessage(message: message)
        }
    }

    // MARK - Exchange data with API
    
    //Sends message to Server
    func sendMessage(message:Message) {
        LocalStorage.sharedInstance.storeMessage(message: message)
        
        let keychain = KeychainSwift()
        
        let username = keychain.get("username")
        let password = keychain.get("password")
        
        let text = message.text
        let touser = self.chat.identifier //ToUserID
        let authentication = Authentication(userName: username!, password: password!)
        let sendMessage = SendMessage(toUserId: touser, text: text, authentication: authentication)
        let dispatchgroup = DispatchGroup()
        
        dispatchgroup.enter()
        TutorScoutApi.shared.send(toSend: sendMessage, jsonUrl: JsonUrl.sendMessage, httpMethod: HTTPMethod.post, typeOfResponse: ResponseType.none) { (error, response, nil) in
            if(error != nil){
                print("Debug-Description: "+error.debugDescription)
                dispatchgroup.leave()
            }
            
            if let response = response {
                if response.statusCode != 200{
                    message.status = .Failed
                }
                else {
                    message.status = .Sent
                }
                print("[MessageSend] StatusCode: \(response.statusCode)")
            }
            
            dispatchgroup.leave()
        }
        
        dispatchgroup.notify(queue: DispatchQueue.main, execute: {
            self.perform(#selector(MessageGateway.updateMessageStatus(message:)), with:message)
        })
    }
    
    //Sends read status to Server
    func sendReadStatusToMessages(messageIds:[String]) {
        if messageIds.count == 0 {
            return
        }
        //TODO
    }
    
    //Sends recieived status to Server
    func sendReceivedStatusToMessages(messageIds:[String]) {
        if messageIds.count == 0 {
            return
        }
        //TODO
    }
    
    //Singleton
    static let sharedInstance = MessageGateway()
    private override init() {}
}
