//
//  ChatListController.swift
//  Tutorscout24 - Chat
//
//  Created by Jan Becker on 31.10.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import UIKit

class ChatController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var  tableView:UITableView!
    var tableData:[Chat] = []
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Ziehen zum refreshen")
        refreshControl.addTarget(self, action: #selector(refreshDataFromServer), for: .valueChanged)
        
        self.setTableView()
        if !GlobalVariables.sharedManager.getMessagesLoaded() {
            GlobalVariables.sharedManager.setMessagesLoaded()
        }
        self.refreshDataFromServer()
        
        NavigationBarItems.initializeNavigationBar(navItem: self.navigationItem, title: "Nachrichten")
    }
    
    override func viewWillAppear(_ animated:Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
    
    //Refreshing Data by pulling from Server
    @objc func refreshDataFromServer(){
        tableData.removeAll()
        tableView.reloadData()
        LocalStorage.sharedInstance.deleteAll()
        self.loadAllChatsFromServer()
        refreshControl.endRefreshing()
    }
    
    //Inits TableView
    func setTableView() {
        self.tableView.addSubview(refreshControl)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView(frame:CGRect(x:0,y: 0,width:self.view.frame.size.width,height: 10))
        self.tableView.backgroundColor = UIColor.clear
    }
    
    
    // MARK - Test Data
    
    func setTest(_ name:String, id:String) {
        let contact = Contact()
        contact.name = name
        contact.identifier = id
        
        let chat = Chat()
        chat.contact = contact
        
        let texts = ["Hallo, ich heiße "+name+"!",
                     "Dies ist eine Testnachricht, welche deutlich länger als die anderen ist.",
                     "Meine ID: "+id]
        
        var lastMessage:Message!
        for text in texts {
            let message = Message()
            message.text = text
            message.sender = .Someone
            message.status = .Received
            message.chatId = chat.identifier
            
            LocalStorage.sharedInstance.storeMessage(message: message)
            lastMessage = message
        }
        
        chat.numberOfUnreadMessages = texts.count
        chat.lastMessage = lastMessage

        self.tableData.append(chat)
    }
    
    
    // MARK - Convert data for table
    
    //Pulling Data from Server
    func loadAllChatsFromServer(){
        var messages = [Message]()
        getSentOrReceivedOrUnreadMessages(jsonUrl: .getSentMessages) { (success, sentMessages) in
            if success == false {
                ConnectionError.shared.showConnectionError(controller: self)
                return
            }

            for sentMessage in sentMessages{
                var message = Message()
                message = message.messageFromServerResponse(sent:true,read:true,messageResponse:sentMessage)
                messages.append(message)
                LocalStorage.sharedInstance.storeMessage(message: message)
            }
            
            self.getSentOrReceivedOrUnreadMessages(jsonUrl: .getReceivedMessages) { (success, receivedMessages) in
                if success == false {
                    ConnectionError.shared.showConnectionError(controller: self)
                    return
                }
                
                for receivedMessage in receivedMessages{
                    var message = Message()
                    message = message.messageFromServerResponse(sent:false,read:true,messageResponse:receivedMessage)
                    messages.append(message)
                    LocalStorage.sharedInstance.storeMessage(message: message)
                }
                
                self.getSentOrReceivedOrUnreadMessages(jsonUrl: .getUnreadMessages) { (success, unreadMessages) in
                    if success == false {
                        ConnectionError.shared.showConnectionError(controller: self)
                        return
                    }
                    
                    for unreadMessage in unreadMessages{
                        var message = Message()
                        message = message.messageFromServerResponse(sent:false,read:false,messageResponse:unreadMessage)
                        messages.append(message)
                        LocalStorage.sharedInstance.storeMessage(message: message)
                    }
                    
                    messages.sort(by: {$0.chatId < $1.chatId})
                    
                    DispatchQueue.main.async {
                        self.createChatsFromData(messages: messages)
                    }
                }
        }
        
        }
    }
    
    //Converting Server Data to Local Chats
    func createChatsFromData(messages: [Message]){
        //Getting Chats
        var chatNames = [String]()
        var lastName = ""
        for message in messages {
            if(message.chatId == lastName){
                continue
            }
            
            chatNames.append(message.chatId)
            lastName = message.chatId
        }
        
        _ = messages.sorted(by: {$0.date.compare($1.date as Date) == ComparisonResult.orderedAscending})
        for chatName in chatNames {
            let chat = Chat()
            let result = messages.lazy.filter {message in message.chatId == chatName}
            
            let contact = Contact()
            contact.name = chatName
            contact.identifier = chatName
            chat.contact = contact
            
            chat.numberOfUnreadMessages = messages.lazy.filter {message in message.sender == MessageSender.Someone && message.status != MessageStatus.Read}.count
            chat.lastMessage = result.last
            
            self.tableData.append(chat)
        }
        self.tableData.sort(by: {$0.lastMessage.date.compare($1.lastMessage.date as Date) == ComparisonResult.orderedDescending})
        if let tv = self.tableView {
            tv.reloadData()
        }
    }

    // MARK - TableViewDataSource

    func tableView(_ tableView:UITableView, numberOfRowsInSection section:NSInteger) -> Int
    {
        let numOfRows: Int = self.tableData.count
        if (numOfRows > 0) {
            tableView.separatorStyle = .singleLine
            tableView.backgroundView = nil
        }
        else {
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text = "Keine Daten verfügbar"
            noDataLabel.textColor = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
            tableView.separatorStyle = .none
        }
        
        return numOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell    {
        let cellIdentifier = "ChatListCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! ChatCell
        cell.chat = self.tableData[indexPath.row]
        
        return cell
    }

    
    // MARK - UITableViewDelegate

    func tableView(_ tableView:UITableView, didSelectRowAt indexPath:IndexPath) {
        self.tableView.deselectRow(at: indexPath as IndexPath, animated:true)
        
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "Message") as! MessageController
        controller.chat = self.tableData[indexPath.row]
        self.navigationController!.pushViewController(controller, animated:true)
    }
    
    
    // MARK - Exchange data with API
    
    func getSentOrReceivedOrUnreadMessages(jsonUrl: JsonUrl,completion: @escaping (_ success: Bool, _ data: [GetMessagesResponse]) -> ()) {
        let keychain = KeychainSwift()
        let username = keychain.get("username")
        let password = keychain.get("password")
        let authentication = Authentication(userName: username!, password: password!)
        
        let getSentMessages = GetMessages(authentication: authentication)
        
        TutorScoutApi.shared.send(toSend: getSentMessages, jsonUrl: jsonUrl, httpMethod: HTTPMethod.post, typeOfResponse: ResponseType.getMessagesResponse) { (error, response, responseData) in
            if(error != nil){
                print("Debug-Description: "+error.debugDescription)
            }
            
            if let resp = response{
                print("[GetMessagesSent] StatusCode: \(resp.statusCode)")
                switch resp.statusCode {
                case 200:
                    if let messagesResponse = responseData as? [GetMessagesResponse] {
                        print("[GetMessagesSent] Status: Daten erfolgreich geladen (\(messagesResponse.count) Messages).")
                        DispatchQueue.main.async {
                            completion(true,messagesResponse)
                        }
                    }
                    else {
                        print("[GetMessagesSent] Status: Fehler beim Konvertieren der Daten vom Server.")
                        DispatchQueue.main.async {
                            completion(false,[GetMessagesResponse]())
                        }
                    }
                    return
                case 409:
                    print("[GetMessagesSent] Status: Keine Daten vorhanden.")
                    DispatchQueue.main.async {
                        completion(true,[GetMessagesResponse]())
                    }
                    return
                default:
                    print("[GetMessagesSent] Status: Unbekannter Fehler beim Laden der Daten.")
                    DispatchQueue.main.async {
                        completion(false,[GetMessagesResponse]())
                    }
                }
            }
        }
    }
}
