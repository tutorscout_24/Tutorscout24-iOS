//
//  MessageController.swift
//  Tutorscout24 - Chat
//
//  Created by Jan Becker on 31.10.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

class MessageControllerInit:UIViewController {
    
    
    var chat:Chat! {
        didSet {
             self.title = self.chat.contact.name
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _ = self.storyboard!.instantiateViewController(withIdentifier: "Message") as! MessageController
    }
}
