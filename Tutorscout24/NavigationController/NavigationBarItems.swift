//
//  NavigationBarItems.swift
//  Tutorscout24
//
//  Created by Jan Becker on 19.10.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import UIKit

class NavigationBarItems {
    //Creates the button for NavigationBar (Change Mode)
    private static func switchButton() -> UIBarButtonItem {
        
        let switchButton = UIBarButtonItem(image: UIImage(named: "switchFill.png"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(NavigationBarItems.modeChanged(_:)))
        
        return switchButton
    }
    
    //Changes from Student to Tutor mode - triggers events
    @objc public static func modeChanged(_ sender:UIBarButtonItem!) {
        GlobalVariables.sharedManager.changeMode()
        let mode = GlobalVariables.sharedManager.getMode()
        
        //Wait for loading complete
        var showReady = false
        var ownReady = false
        GlobalVariables.sharedManager.events.listenTo(eventName: "showReady") {
            showReady = true
        }
        GlobalVariables.sharedManager.events.listenTo(eventName: "ownReady") {
            ownReady = true
        }
        GlobalVariables.sharedManager.events.trigger(eventName: "mc", information: "Mode changed!");
        
        UINavigationBar.appearance().barTintColor = NavigationBarItems.uicolorFromHex(rgbValue: mode  ? 0xdb2e02 : 0x2980b9)
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        DispatchQueue.global(qos: .userInitiated).async {
            while(!showReady){
                usleep(10000)
            }
            GlobalVariables.sharedManager.events.listenTo(eventName: "isAlive!", action: {
                print("isAlive!")
                while(!ownReady){
                    usleep(10000)
                }
            })
            GlobalVariables.sharedManager.events.trigger(eventName: "isAlive?", information: "Are you alive?");

            DispatchQueue.main.async {
                UIApplication.shared.endIgnoringInteractionEvents()
                //Message
                let modeName = mode ? "Student" : "Tutor"
                let alert = UIAlertController(title: "Modus gewechselt", message: "Der aktuelle Modus: \(modeName)", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: nil))
                
                let tabBarController = UIApplication.shared.windows[0].rootViewController
                
                tabBarController?.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    //Inits title and button for all NavigationBars
    public static func initializeNavigationBar(navItem: UINavigationItem, title: String) {
        navItem.title = title
        navItem.rightBarButtonItem = switchButton()
    }
    
    //Create UIColor from HexValue
    public static func uicolorFromHex(rgbValue:UInt32)->UIColor{
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)

    }
    
    //Change NavigationBar Design for all NavigationBars
    public static func setNavigationBarAppearance() {
        let navigationBarAppearance = UINavigationBar.appearance()
        
        navigationBarAppearance.tintColor = uicolorFromHex(rgbValue: 0xFFFFFF)
        navigationBarAppearance.barTintColor = NavigationBarItems.uicolorFromHex(rgbValue: 0xdb2e02)
        navigationBarAppearance.isTranslucent = false
        
        navigationBarAppearance.titleTextAttributes = [kCTFontAttributeName as NSAttributedStringKey: UIFont.init(name: "neuropol", size: 20)!,NSAttributedStringKey.foregroundColor:UIColor.white]
    }
}
