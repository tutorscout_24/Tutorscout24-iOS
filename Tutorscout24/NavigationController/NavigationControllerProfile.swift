//
//  NavigationControllerProfile.swift
//  Tutorscout24
//
//  Created by Jan Becker on 13.12.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import Foundation
import UIKit

class NavigationControllerProfile: UINavigationController {
    
    @IBOutlet weak var navProfile: UINavigationBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GlobalVariables.sharedManager.events.listenTo(eventName: "mc") {
            let color = NavigationBarItems.uicolorFromHex(rgbValue: GlobalVariables.sharedManager.getMode()  ? 0xdb2e02 : 0x2980b9)
            
            self.navProfile.barTintColor = color
        }
    }
}

