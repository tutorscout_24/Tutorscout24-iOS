//
//  DetailViewControllerOwn.swift
//  Tutorscout24
//
//  Created by Felix Schillinger on 08.12.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import UIKit

class DetailViewControllerOwn: UIViewController {

    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var createDate: UILabel!
    @IBOutlet weak var expireDate: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    
    var myOfferOrRequestRepsonse: GetMyOfferOrRequestsResponse!
    var parentVC: TableViewControllerOwn!
    var deletingWasSuccess = false
    var dateString: String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupChangeMode()
        setUpLabelsAndButtons(roundnessButtons: 10, alpha: 0.5)
    }
    
    //converts the timestamp to a more user friendly time stamp
    func convertTimeStampToString(timeStamp: String) -> String{
        let dateFormatter = DateFormatter()
        let calendar = Calendar.current
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        var dateString = timeStamp
        if let datePublished = dateFormatter.date(from: dateString) {
            
            let day = String(format: "%02d", calendar.component(.day, from: datePublished))
            let month = String(format: "%02d", calendar.component(.month, from: datePublished))
            let year = String(format: "%04d", calendar.component(.year, from: datePublished))
            let hour = String(format: "%02d", calendar.component(.hour, from: datePublished))
            let minute = String(format: "%02d", calendar.component(.minute, from: datePublished))
            print("\(day).\(month).\(year) at \(hour):\(minute)")
            dateString = day + "." + month + "." + year + " at " + hour + ":" + minute
        }
        return dateString
    }
    
    //sets all labels and buttons up
    func setUpLabelsAndButtons(roundnessButtons: Int, alpha: Float){
        deleteButton.layer.cornerRadius = CGFloat(roundnessButtons)
        subjectLabel.lineBreakMode = .byWordWrapping
        subjectLabel.numberOfLines = 0
        subjectLabel.backgroundColor = UIColor.white.withAlphaComponent(CGFloat(alpha))
        subjectLabel.layer.masksToBounds = true
        subjectLabel.layer.cornerRadius = CGFloat(roundnessButtons)
        
        descriptionLabel.lineBreakMode = .byWordWrapping
        descriptionLabel.numberOfLines = 0
        descriptionLabel.backgroundColor = UIColor.white.withAlphaComponent(CGFloat(alpha))
        descriptionLabel.layer.masksToBounds = true
        descriptionLabel.layer.cornerRadius = CGFloat(roundnessButtons)
        
        setLableAndButtonTexts()
        
        
        
        
    }
    
    //writes content into labels
    func setLableAndButtonTexts(){
        subjectLabel.text = myOfferOrRequestRepsonse.subject
        descriptionLabel.text = myOfferOrRequestRepsonse.text
        createDate.text = "Created: " + convertTimeStampToString(timeStamp: myOfferOrRequestRepsonse.creationdate)
        expireDate.text = "Expires: " + convertTimeStampToString(timeStamp: myOfferOrRequestRepsonse.end)
        deleteButton.setTitle("Delete", for: .normal)
        
    }
    
    //deletes the offer/request 
    @IBAction func deleteButtonTapped(_ sender: UIButton) {
        print("deleteButton pressed")
        DeleteHandler.shared.delete(tutoringId: myOfferOrRequestRepsonse.tutoringid) { (success) in
            if(!success){
                self.showAlert(title: "Deleting Failed", message: "Please try again later", buttonText: "OK")
            }else{
                self.parentVC.offerOrRequestWasDeleted = true
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func showAlert(title: String, message: String, buttonText: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonText, style: .default, handler: nil))
        self.present(alert, animated: true, completion: {
            self.dismiss(animated: true, completion: nil)
        })
    }
    
    func setupChangeMode(){
        GlobalVariables.sharedManager.events.listenTo(eventName: "mc") { (information: Any?) in
            self.changeMode()
        }
    }
    
    func changeMode(){
        let color = NavigationBarItems.uicolorFromHex(rgbValue: GlobalVariables.sharedManager.getMode()  ? 0xdb2e02 : 0x0032E9)
        
        deleteButton.backgroundColor = color
    }

}
