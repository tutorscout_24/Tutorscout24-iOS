//
//  DeleteHandler.swift
//  Tutorscout24
//
//  Created by Felix Schillinger on 09.12.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import Foundation

//class for deleting offer/request -> handles error
class DeleteHandler{
    static let shared: DeleteHandler = DeleteHandler()
    private init() {}
    
    func delete(tutoringId: Int, completion: @escaping (_ success: Bool) -> ()){
        let keychain = KeychainSwift()
        let username = keychain.get("username")!
        let password = keychain.get("password")!
        let authentication = Authentication(userName: username, password: password)
        let deleteTutoring = DeleteTutoring(tutoringId: tutoringId, authentication: authentication)
        
        TutorScoutApi.shared.send(toSend: deleteTutoring, jsonUrl: .deleteTutoring, httpMethod: .delete, typeOfResponse: .none) { (error, response, nil) in
            if(error != nil){
                print("Error during deleting tutoring: \(error.debugDescription)")
            }
            
            if let response = response{
                print("Status code deleting tutoring: \(response.statusCode)")
                DispatchQueue.main.async {
                    if(response.statusCode == 200){
                        completion(true)
                    }else{
                        completion(false)
                    }
                }
            }
            
        }
    }
}
