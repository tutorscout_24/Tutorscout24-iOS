//
//  ViewControllerOwn.swift
//  Tutorscout24
//
//  Created by Jan Becker on 19.10.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import UIKit

class ViewControllerOwn: UIViewController {
    var viewDidLayoutSubviewsAlready = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NavigationBarItems.initializeNavigationBar(navItem: self.navigationItem, title: "Tutorien")
    }
    
    override func viewDidLayoutSubviews() {
        print("viewDidLayoutSubviews in Owncontroller was called")
        if(!viewDidLayoutSubviewsAlready){
            viewDidLayoutSubviewsAlready = true
            addTableViewController(width: self.view.frame.width, height: self.view.frame.height)
        }
    }
    
    //adds the TableViewControllers view to the controller
    func addTableViewController(width: CGFloat, height: CGFloat){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ownTableViewController = storyboard.instantiateViewController(withIdentifier: "tableViewControllerOwn")
        self.addChildViewController(ownTableViewController)
        let ownTableView = ownTableViewController.view!
        
        self.view.addSubview(ownTableView)
        ownTableView.frame.size.width = width
        ownTableView.frame.size.height = height
        ownTableViewController.didMove(toParentViewController: self)
    }
}
