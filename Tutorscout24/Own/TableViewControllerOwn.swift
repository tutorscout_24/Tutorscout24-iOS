//
//  TableViewControllerOwn.swift
//  Tutorscout24
//
//  Created by Felix Schillinger on 08.12.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import UIKit

class TableViewControllerOwn: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    
    var myOfferOrRequestRepsonse: [GetMyOfferOrRequestsResponse] = [GetMyOfferOrRequestsResponse]()
    var currentIndex: Int = 0
    var lastOfferOrRequestResponseCountBeforeUpdate: Int = 0
    var firstLoad: Bool = true
    var viewDidLayoutSubviewsAlready: Bool = false
    var offerOrRequestWasDeleted: Bool = false
    private var jsonURL: JsonUrl = .myRequests
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupChangeMode()
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Ziehen zum refreshen")
        refreshControl.addTarget(self, action: #selector(refreshDataFromServer), for: .valueChanged)
        
        tableView.addSubview(refreshControl)
        
        tableView.tableFooterView = UIView()
        
        loadOfferOrRequests()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 80
        
    }
    
    //fills the tableview with content
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let numOfRows: Int = myOfferOrRequestRepsonse.count
        if (numOfRows > 0) {
            tableView.separatorStyle = .singleLine
            tableView.backgroundView = nil
        }
        else{
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text = "Keine Daten verfügbar"
            noDataLabel.textColor = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
            tableView.separatorStyle = .none
        }
        return numOfRows
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        
        cell.subjectLable.text = myOfferOrRequestRepsonse[indexPath.row].subject
        cell.descriptionLable.text = myOfferOrRequestRepsonse[indexPath.row].text
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentIndex = indexPath.row
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "segue", sender: self)
    }
    
    //laods offer/request from server
    func loadOfferOrRequests(){
        let keychain = KeychainSwift()
        let username = keychain.get("username")!
        let password = keychain.get("password")!
        lastOfferOrRequestResponseCountBeforeUpdate = myOfferOrRequestRepsonse.count
        let getMyOfferOrrequest = GetMyOfferOrRequests(authentication: Authentication(userName: username, password: password))
        
        activityIndicator.startAnimating()
        
        //get data from server
        TutorScoutApi.shared.send(toSend: getMyOfferOrrequest, jsonUrl: jsonURL, httpMethod: HTTPMethod.post, typeOfResponse: ResponseType.getMyOfferOrRequestsResponse) { (error, response, responseData) in
            if(error != nil){
                print("some error occured: \(error.debugDescription)")
                DispatchQueue.main.async {
                    self.reloadDataAndEndLoadingAnimation()
                    self.refreshControl.endRefreshing()
                    ConnectionError.shared.showConnectionError(controller: self)
                }
            }
            
            if let response = response{
                print("status code: \(response.statusCode)")
                if(response.statusCode != 200){
                    self.activityIndicator.stopAnimating()
                }
            }
            
            if let offersResponse = responseData as? [GetMyOfferOrRequestsResponse] {
                print("offerresponse.count: \(offersResponse.count)")
                self.myOfferOrRequestRepsonse.append(contentsOf: offersResponse)
                DispatchQueue.main.async {
                    self.reloadDataAndEndLoadingAnimation()
                }
            }
        }
        
        
    }
    
    //loads data in tableview (if there is data)
    func reloadDataAndEndLoadingAnimation(){
        if(!firstLoad){
            let dataToAddCount = myOfferOrRequestRepsonse.count - lastOfferOrRequestResponseCountBeforeUpdate
            if(dataToAddCount == 0){
                activityIndicator.stopAnimating()
                return
            }
            
            var indexPaths = [IndexPath]()
            for i in 0..<dataToAddCount {
                indexPaths.append(IndexPath(row: lastOfferOrRequestResponseCountBeforeUpdate + i, section: 0))
            }
            
            tableView.beginUpdates()
            tableView.insertRows(at: indexPaths, with: .bottom)
            tableView.endUpdates()
        }else{
            firstLoad = false
        }
        activityIndicator.stopAnimating()
        
    }
    
    //deletes all content in tableview and reload data from server
    @objc func refreshDataFromServer(){
        myOfferOrRequestRepsonse.removeAll()
        lastOfferOrRequestResponseCountBeforeUpdate = 0
        tableView.reloadData()
        loadOfferOrRequests()
        refreshControl.endRefreshing()
        GlobalVariables.sharedManager.events.trigger(eventName: "ownReady", information: "ownView is Ready!")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detailViewController = segue.destination as! DetailViewControllerOwn
        
        detailViewController.myOfferOrRequestRepsonse = self.myOfferOrRequestRepsonse[currentIndex]
        detailViewController.parentVC = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(offerOrRequestWasDeleted){
            offerOrRequestWasDeleted = false
            refreshDataFromServer()
        }
    }
    
    func setupChangeMode(){
        GlobalVariables.sharedManager.events.listenTo(eventName: "mc") { (information: Any?) in
            self.changeMode(isTutor: !GlobalVariables.sharedManager.getMode())
        }
        GlobalVariables.sharedManager.events.listenTo(eventName: "isAlive?") {
            GlobalVariables.sharedManager.events.trigger(eventName: "isAlive!")
        }
    }
    
    func changeMode(isTutor: Bool){
        print("is in changeMode function with isTutor: \(isTutor)")
        if(isTutor){
            jsonURL = .myOffers
        }else{
            jsonURL = .myRequests
        }
        refreshDataFromServer()
    }
}
