//
//  UpdateUserViewController.swift
//  Tutorscout24
//
//  Created by Felix Schillinger on 09.12.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import UIKit
import MapKit

class UpdateUserViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var userId: SkyFloatingLabelTextField!
    @IBOutlet weak var firstName: SkyFloatingLabelTextField!
    @IBOutlet weak var lastName: SkyFloatingLabelTextField!
    @IBOutlet weak var password: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordRepeat: SkyFloatingLabelTextField!
    @IBOutlet weak var living: SkyFloatingLabelTextField!
    @IBOutlet weak var maxGraduation: SkyFloatingLabelTextField!
    @IBOutlet weak var note: SkyFloatingLabelTextField!
    @IBOutlet weak var email: SkyFloatingLabelTextField!
    @IBOutlet weak var city: SkyFloatingLabelTextField!
    @IBOutlet weak var address: SkyFloatingLabelTextField!
    @IBOutlet weak var activitiyIndicator: NVActivityIndicatorView!
    @IBOutlet weak var changeBtn: UIButton!
    
    var textFields: [SkyFloatingLabelTextField] = [SkyFloatingLabelTextField]()
    var emailBeforeUpdate: String!
    var myUserInfoResponse: MyUserInfoResponse!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupChangeMode()
        setupTextFieldsAndButton(roundness: 10)
        getUserInfos()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupTextFieldsAndButton(roundness: Float){
        textFields.append(contentsOf: [userId, firstName, lastName, password, passwordRepeat, living, maxGraduation, note, email, city, address])
        
        for textfield in textFields{
            textfield.delegate = self
        }
        userId.placeholder = "Username"
        userId.isUserInteractionEnabled = false
        firstName.placeholder = "Vorname*"
        lastName.placeholder = "Nachname*"
        password.placeholder = "Passwort*"
        passwordRepeat.placeholder = "Wiederholtes Passwort*"
        living.placeholder = "Wohnort"
        maxGraduation.placeholder = "Höchster Abschluss"
        note.placeholder = "Notiz"
        email.placeholder = "E-Mail*"
        city.placeholder = "Stadt*"
        address.placeholder = "Adresse*"
        
        if OffersOrRequests.shared.getUseCoreLocation(){
            city.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            address.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        }
        
        let color = NavigationBarItems.uicolorFromHex(rgbValue: GlobalVariables.sharedManager.getMode()  ? 0xdb2e02 : 0x2980b9)
        changeBtn.backgroundColor = color
        
        changeBtn.layer.cornerRadius = CGFloat(roundness)
    }
    
    //shows alert if city and address textfield want to be changed by the user
    @objc func textFieldDidChange(_ textField: UITextField) {
        showAlert(title: "Ändere deine Einstellungen",message: "Du benutzt GPS Lokalisierung um deinen Standort zu ermitteln. Um manuell deine Standort zu ändern, ändere bitte deine privaten Einstellungen.", buttonText: "OK")
        city.text = CurrentLocation.shared.getCity()
        address.text = CurrentLocation.shared.getStreet()
    }
    
    //gets all infos from user from server
    func getUserInfos(){
        activitiyIndicator.startAnimating()
        let keychain = KeychainSwift()
        let username = keychain.get("username")!
        let password = keychain.get("password")!
        let authentication = Authentication(userName: username, password: password)
        let myUserInfo = MyUserInfo(authentication: authentication)
        
        //fetch data from server
        TutorScoutApi.shared.send(toSend: myUserInfo, jsonUrl: .myUserInfo, httpMethod: .post, typeOfResponse: .myUserInfoResponse) { (error, response, myUserInfoResponse) in
            if(error != nil){
                print("Error getting myUserInfoResponse (in UpdateUserViewController): \(error.debugDescription)")
                DispatchQueue.main.async {
                    self.activitiyIndicator.stopAnimating()
                    self.disableTextfields()
                    ConnectionError.shared.showConnectionError(controller: self)
                }
            }
            
            if let response = response{
                print("Status Code: \(response.statusCode)")
            }
            
            if let myUserInfoResponse = myUserInfoResponse as? MyUserInfoResponse{
                self.myUserInfoResponse = myUserInfoResponse
                DispatchQueue.main.async {
                    self.loadDataInTextfields()
                }
            }
            
        }
        
        
        
    }
    
    func disableTextfields(){
        for textfield in textFields {
            textfield.isUserInteractionEnabled = false
        }
    }
    
    //wirtes user data in each textfield
    func loadDataInTextfields(){
        let location = CurrentLocation.shared
        let password = KeychainSwift().get("password")!
        
        userId.text = myUserInfoResponse.userid
        firstName.text = myUserInfoResponse.firstName
        lastName.text = myUserInfoResponse.lastName
        self.password.text = password
        passwordRepeat.text = password
        living.text = myUserInfoResponse.placeOfResidence
        maxGraduation.text = myUserInfoResponse.maxGraduation
        note.text = myUserInfoResponse.description
        email.text = myUserInfoResponse.email
        emailBeforeUpdate = myUserInfoResponse.email
        
        city.text = location.getCity()
        address.text = location.getStreet()
        
        
        activitiyIndicator.stopAnimating()
    }
    
    
    //performs an update
    func performUpdate(){
        if let firstname = firstName.text, !firstname.isEmpty, let lastname = lastName.text, !lastname.isEmpty, let pw = password.text, !pw.isEmpty, let pwr = passwordRepeat.text, !pwr.isEmpty, let mail = email.text, !mail.isEmpty, let city = city.text, !city.isEmpty, let address = address.text, !address.isEmpty{
            if(pw != pwr){
                showAlert(title: "Passwörter stimmen nicht überein", message: "Dein angegebenes Passwort und das wiederholte stimmen nicht überein", buttonText: "OK")
            }else{ //password matches the repeated
                activitiyIndicator.startAnimating()
                //check if entered location is valid
                CLGeocoder().geocodeAddressString(city + " " + address, completionHandler: { (placemarks, error) in
                    if error != nil{
                        self.showAlert(title: "Inkorrekte Adresse!", message: "Die Angegeben aktuelle Adresse scheint nicht zu existieren", buttonText: "OK")
                    }else if let place = placemarks?[0]{
                        var cityString: String!
                        var streetString: String!
                        if let city = place.locality, let street = place.thoroughfare, let location = place.location{
                            OffersOrRequests.shared.setCoordiantes(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                            cityString = city
                            streetString = street
                        }else{return}
                        if let streetNumber = place.subThoroughfare{
                            streetString = streetString + " " + streetNumber
                        }
                        self.city.text = cityString
                        self.address.text = streetString
                        CurrentLocation.shared.setCityAndStreet(city: cityString, street: streetString)
                        OffersOrRequests.shared.getTableController().refreshDataFromServer()
                    }
                })
                sendUpdateToServer(pw: pw, firstName: firstname, lastName: lastname, email: mail)
            }
        }else{//some reqiured fields are missing
            showAlert(title: "Fehlende Felder", message: "Nicht alle Pflichtfelder (*) sind ausgefüllt", buttonText: "OK")
        }
    }
    
    //sends the new data to server
    func sendUpdateToServer(pw: String, firstName: String, lastName: String, email: String){
        let updatedUser: UpdateUser!
        let note: String = self.note.text!
        let living: String = self.living.text!
        let graduation: String = self.maxGraduation.text!
        
        let keychain = KeychainSwift()
        let username = keychain.get("username")!
        let password = keychain.get("password")!
        
        let authentication = Authentication(userName: username, password: password)
        
        if(email == emailBeforeUpdate){
            updatedUser = UpdateUser(password: pw, firstName: firstName, lastName: lastName, birthdate: myUserInfoResponse.dayOfBirth, gender: myUserInfoResponse.gender, email: nil, note: note, placeOfResidence: living, maxGraduation: graduation, authentication: authentication)
        }else{
            updatedUser = UpdateUser(password: pw, firstName: firstName, lastName: lastName, birthdate: myUserInfoResponse.dayOfBirth, gender: myUserInfoResponse.gender, email: email, note: note, placeOfResidence: living, maxGraduation: graduation, authentication: authentication)
        }
        
        //sending data to server
        TutorScoutApi.shared.send(toSend: updatedUser, jsonUrl: .updateUser, httpMethod: .put, typeOfResponse: .none) { (error, response, nil) in
            if(error != nil){
                print("Error during sending update: \(error.debugDescription)")
                DispatchQueue.main.async {
                    self.handleFailure()
                }
            }
            
            if let response = response{
                print("Status Code: \(response.statusCode)")
                if(response.statusCode == 500){
                    print("Email address is already in use!")
                    DispatchQueue.main.async {
                        self.showAlert(title: "Email Adresse in Benutzung", message: "Die angegebene Email Adresse ist schon in Benutzung", buttonText: "OK")
                    }
                }else if(response.statusCode != 200){
                    DispatchQueue.main.async {
                        self.handleFailure()
                    }
                }else{
                    DispatchQueue.main.async {
                        self.activitiyIndicator.stopAnimating()
                        self.showAlert(title: "Account aktualisiert", message: "Dein Account wurde erfolgreich aktualisiert!", buttonText: "OK")
                    }
                }
            }
        }
    }
    
    func handleFailure(){
        activitiyIndicator.stopAnimating()
        
        showAlert(title: "Update Fehlgeschlagen", message: "Es sind Probleme während des Updates deines Accounts aufgetreten. Probiere es später erneut.", buttonText: "OK")
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func changeBtnPressed(_ sender: UIButton) {
        print("changedBtnPressed")
        performUpdate()
        
    }
    
    func showAlert(title: String, message: String, buttonText: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonText, style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func setupChangeMode(){
        GlobalVariables.sharedManager.events.listenTo(eventName: "mc") { (information: Any?) in
            self.changeMode()
        }
    }
    
    func changeMode(){
        let color = NavigationBarItems.uicolorFromHex(rgbValue: GlobalVariables.sharedManager.getMode()  ? 0xdb2e02 : 0x0032E9)
        
        changeBtn.backgroundColor = color
    }

}
