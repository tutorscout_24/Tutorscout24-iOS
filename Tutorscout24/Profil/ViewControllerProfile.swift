//
//  ViewControllerProfile.swift
//  Tutorscout24
//
//  Created by Jan Becker on 19.10.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import UIKit

class ViewControllerProfile: UIViewController {
    
    @IBOutlet weak var updateUserBtn: UIButton!
    @IBOutlet weak var deleteUserBtn: UIButton!
    @IBOutlet weak var logOutBtn: UIButton!
    
    var buttons: [UIButton] = [UIButton]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupChangeMode()
        NavigationBarItems.initializeNavigationBar(navItem: self.navigationItem, title: "Profil")
        
        buttons.append(contentsOf: [updateUserBtn, deleteUserBtn, logOutBtn])
        setupButtons(roundness: 10)
    }
    
    func setupButtons(roundness: Float){
        for button in buttons {
            button.layer.cornerRadius = CGFloat(roundness)
        }
        changeMode()
    }
    
    @IBAction func updateUserPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "segue", sender: self)
    }
    
    //delets the users account
    @IBAction func deleteUserPressed(_ sender: UIButton) {
        //ask if the user is sure
        let title = "Account Löschen?"
        let message = "Bist du sicher, dass du deinen Account löschen möchtest? Diese Änderung kann nicht Rückgängig gemacht werden"
        let buttonTextYes = "Ja"
        let buttonTextNo = "Nein"
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        //canceld deleting
        alert.addAction(UIAlertAction(title: buttonTextNo, style: .cancel, handler: nil))
        //delet account
        alert.addAction(UIAlertAction(title: buttonTextYes, style: .destructive, handler: { (_) in
            let keychain = KeychainSwift()
            let username = keychain.get("username")!
            let password = keychain.get("password")!
            let auth = Authentication(userName: username, password: password)
            let deleteUser = DeleteUser(authentication: auth)
            TutorScoutApi.shared.send(toSend: deleteUser, jsonUrl: .delete, httpMethod: HTTPMethod.delete, typeOfResponse: ResponseType.none, completion: { (error, response, nil) in
                if error != nil {
                    DispatchQueue.main.async {
                        ConnectionError.shared.showConnectionError(controller: self)
                    }
                }else if let response = response {
                    if (response.statusCode == 200) {
                        self.cleanUserData()
                    }
                }
            })
        }))
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    //logs the user out
    @IBAction func logoutPressed(_ sender: UIButton) {
        print("logout button pressed")
        cleanUserData()
    }
    
    //clean all data the was produced during session from user
    func cleanUserData(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginVC = storyboard.instantiateViewController(withIdentifier: "loginVC")
        
        self.present(loginVC, animated: true) {
            //clear keychain
            let keychain = KeychainSwift()
            keychain.clear()
            //reset other data
            OffersOrRequests.shared.resetData()
        }
    }
    
    func setupChangeMode(){
        GlobalVariables.sharedManager.events.listenTo(eventName: "mc") { (information: Any?) in
            self.changeMode()
        }
    }
    
    func changeMode(){
        let color = NavigationBarItems.uicolorFromHex(rgbValue: GlobalVariables.sharedManager.getMode()  ? 0xdb2e02 : 0x2980b9)
        
        buttons[0].backgroundColor = color
        buttons[1].backgroundColor = color
    }
}
