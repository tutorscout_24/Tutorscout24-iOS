//
//  ConnectionError.swift
//  Tutorscout24
//
//  Created by Felix Schillinger on 28.12.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import Foundation

class ConnectionError{
    static let shared: ConnectionError = ConnectionError()
    private init() {}
    
    //shows the error message if there is something wrong with the internet connection
    func showConnectionError(controller: UIViewController){
        let title = "Du bist offline"
        let message = "Es scheint als wäre keine Internetverbindung vorhanden. Probiere es später nochmal."
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        controller.present(alert, animated: true, completion: nil)
    }
}
