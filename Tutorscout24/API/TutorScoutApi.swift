//
//  TutorScoutApi.swift
//  Tutorscout24
//
//  Created by Felix Schillinger on 14.11.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import Foundation

class TutorScoutApi{
    
    static let shared: TutorScoutApi = TutorScoutApi()
    private init() {}
    
    //responsible for GET requests
    func get<T: Codable>(typeOfResponse: T, jsonUrl: JsonUrl, completion: @escaping (Any?) -> ()){
        let jsonUrlString: String = jsonUrl.rawValue
        
        guard let url = URL(string: jsonUrlString) else {return}
        
        //connects to the api
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            //error checking
            if error != nil{
                print("something went wrong during connecting to API")
                print(error.debugDescription)
            }
            
            guard let data = data else {return}
            
            do{
                let apiResponse = try JSONDecoder().decode(type(of: typeOfResponse), from: data)
                completion(apiResponse)
            } catch let error {
                print(error.localizedDescription)
            }
            }.resume()
    }
    
    //respobsible for POST requests
    func send<T: Codable>(toSend: T, jsonUrl: JsonUrl, httpMethod: HTTPMethod, typeOfResponse: ResponseType, completion: @escaping (_ error: Error?,_ response: HTTPURLResponse?,_ responseData: Any?) -> ()){
        var responseDataC: Any?
        var responseC: HTTPURLResponse?
        
        //setup specifiy request
        let jsonUrlString: String = jsonUrl.rawValue
        
        guard let url = URL(string: jsonUrlString) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        //encode request to JSON
        do {
            let jsonBody = try JSONEncoder().encode(toSend)
            request.httpBody = jsonBody
        } catch  { }
        
        //connects to api
        URLSession.shared.dataTask(with: request) { (responseData, response, responseError) in
            if responseError != nil{
                print("Something went wrong during connecting to API!")
                completion(responseError, nil, nil)
                return
            }
            
            if let response = response as? HTTPURLResponse{
                print("Response is: \(response)")
                print("Status code: \(response.statusCode)")
                responseC = response
            }
            
            //get data if there is some data
            guard let data = responseData else {
                completion(nil, responseC, nil)
                return
            }
            
            
            //print json if there is data
            do{
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
            } catch let error {
                print("Error description to JSON: \(error.localizedDescription)")
            }
            
            // nesteted function decode if there is data
            func decode<T: Codable>(responseType: T.Type){
                do{
                    let apiResponse = try JSONDecoder().decode(responseType, from: data)
                    completion(nil, responseC, apiResponse)
                } catch let error {
                    print("Error description during decoding: \(error.localizedDescription)")
                    completion(error, responseC, nil)
                }
            }
            
            //checks which type if response is expected
            switch typeOfResponse{
            case .myUserInfoResponse:
                decode(responseType: MyUserInfoResponse.self)
            case .userInfoResponse:
                decode(responseType: UserInfoResponse.self)
            case .getMessagesResponse:
                decode(responseType: [GetMessagesResponse].self)
            case .getMyOfferOrRequestsResponse:
                decode(responseType: [GetMyOfferOrRequestsResponse].self)
            case .getOfferOrRequestsResponse:
                decode(responseType: [GetOfferOrRequestsResponse].self)
            default:
                print("No response data requested!")
                completion(nil, responseC, nil)
                return
            }
            
        }.resume()
    }
    
    
}
