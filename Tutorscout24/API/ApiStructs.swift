//
//  ApiStructs.swift
//  Tutorscout24
//
//  Created by Felix Schillinger on 14.11.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import Foundation

//structures that are neccessary for decode/encode from/to JSON

//MARK: Users

//GET
struct Info: Codable {
    let userCount: Int
}

//POST
struct Create: Codable {
    let userName: String
    let password: String
    let firstName: String
    let lastName: String
    let birthdate: String
    let gender: String
    let email: String
    let note: String
    let placeOfResidence: String
    let maxGraduation: String
}

//POST
struct CheckAuthentication: Codable {
    let authentication: Authentication
}

//POST
struct Authentication: Codable {
    let userName: String
    let password: String
}

//POST

struct UserInfo: Codable {
    let userToFind: String
    let authentication: Authentication
}

struct UserInfoResponse: Codable {
    let userid: String
    let firstName: String
    let lastName: String
    let age: Int
    let gender: String
    let description: String
    let maxGraduation: String
}

//POST
struct MyUserInfo: Codable {
    let authentication: Authentication
}

struct MyUserInfoResponse: Codable {
    let userid: String
    let firstName: String
    let lastName: String
    let dayOfBirth: String
    let gender: String
    let description: String
    let maxGraduation: String
    let email: String
    let placeOfResidence: String
}

//POST
struct CompleteRegistration: Codable {
    let registrationCode: Int
}

//PUT
struct UpdateUser: Codable {
    let password: String
    let firstName: String
    let lastName: String
    let birthdate: String
    let gender: String
    let email: String?
    let note: String
    let placeOfResidence: String
    let maxGraduation: String
    let authentication: Authentication
}

//POST
struct DeleteUser: Codable {
    let authentication: Authentication
}

//MARK: Message

//POST
struct SendMessage: Codable {
    let toUserId: String
    let text: String
    let authentication: Authentication
}

//DELETE
struct DeleteMessage: Codable {
    let messageId: Int  //->In swagger it is an string?!
    let authentication: Authentication
}

//POST
struct GetMessages: Codable {
    let authentication: Authentication
}

struct GetMessagesResponse: Codable {
    let messageId: Int
    let datetime: String
    let fromUserId: String
    let toUserId: String
    let text: String
}


//MARK: Tutoring

//POST
struct CreateOfferOrRequest: Codable {
    let subject: String
    let text: String
    let duration: Int
    let latitude: Double
    let longitude: Double
    let authentication: Authentication
}

//POST
struct GetMyOfferOrRequests: Codable {
    let authentication: Authentication
}

struct GetMyOfferOrRequestsResponse: Codable {
    let tutoringid: Int
    let creationdate: String
    let createruserid: String
    let subject: String
    let text: String
    let type: String
    let end: String
    let latitude: Double
    let longitude: Double
}

//POST
struct GetOfferOrRequests: Codable {
    let latitude: Double
    let longitude: Double
    let rangeKm: Int
    let rowLimit: Int
    let rowOffset: Int
    let authentication: Authentication
}

struct GetOfferOrRequestsResponse: Codable {
    let tutoringId: Int
    let userName: String
    let creationDate: String
    let subject: String
    let text: String
    let expirationDate: String
    let latitude: Double
    let longitude: Double
    let distanceKm: Double
}

//DELETE
struct DeleteTutoring: Codable {
    let tutoringId: Int
    let authentication: Authentication
}



