//
//  JsonUrls.swift
//  Tutorscout24
//
//  Created by Felix Schillinger on 14.11.17.
//  Copyright © 2017 DHBW TINF2016 Team B. All rights reserved.
//

import Foundation

//api urls to specific api
enum JsonUrl: String{
    //user
    case info = "http://tutorscout24.vogel.codes:3000/tutorscout24/api/v1/user/info"
    case createUser = "http://tutorscout24.vogel.codes:3000/tutorscout24/api/v1/user/create"
    case checkAuthentication = "http://tutorscout24.vogel.codes:3000/tutorscout24/api/v1/user/checkAuthentication"
    case userInfo = "http://tutorscout24.vogel.codes:3000/tutorscout24/api/v1/user/userInfo"
    case myUserInfo = "http://tutorscout24.vogel.codes:3000/tutorscout24/api/v1/user/myUserInfo"
    case comleteRegistration = "http://tutorscout24.vogel.codes:3000/tutorscout24/api/v1/user/completeRegistration"
    case updateUser = "http://tutorscout24.vogel.codes:3000/tutorscout24/api/v1/user/updateUser"
    case delete = "http://tutorscout24.vogel.codes:3000/tutorscout24/api/v1/user/delete"
    //message
    case sendMessage = "http://tutorscout24.vogel.codes:3000/tutorscout24/api/v1/message/sendMessage"
    case deleteMessage = "http://tutorscout24.vogel.codes:3000/tutorscout24/api/v1/message/deleteMessage"
    case getSentMessages = "http://tutorscout24.vogel.codes:3000/tutorscout24/api/v1/message/getSentMessages"
    case getReceivedMessages = "http://tutorscout24.vogel.codes:3000/tutorscout24/api/v1/message/getReceivedMessages"
    case getUnreadMessages = "http://tutorscout24.vogel.codes:3000/tutorscout24/api/v1/message/getUnreadMessages"
    //tutoring
    case createOffer = "http://tutorscout24.vogel.codes:3000/tutorscout24/api/v1/tutoring/createOffer"
    case createRequest = "http://tutorscout24.vogel.codes:3000/tutorscout24/api/v1/tutoring/createRequest"
    case myRequests = "http://tutorscout24.vogel.codes:3000/tutorscout24/api/v1/tutoring/myRequests"
    case myOffers = "http://tutorscout24.vogel.codes:3000/tutorscout24/api/v1/tutoring/myOffers"
    case requests = "http://tutorscout24.vogel.codes:3000/tutorscout24/api/v1/tutoring/requests"
    case offers = "http://tutorscout24.vogel.codes:3000/tutorscout24/api/v1/tutoring/offers"
    case deleteTutoring = "http://tutorscout24.vogel.codes:3000/tutorscout24/api/v1/tutoring/delete"
}

//type of HTTP method
enum HTTPMethod: String{
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}

//type of expected response
enum ResponseType{
    case userInfoResponse
    case myUserInfoResponse
    case getMessagesResponse
    case getMyOfferOrRequestsResponse
    case getOfferOrRequestsResponse
    case none
}

